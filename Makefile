# TODO: setup.py

doc:
	epydoc -v -n ScenarioGenerator ./scenario_generator/*.py ./*.py -o doc

docclean:
	rm -rf doc.tar.gz doc.zip doc

lint:
	pylint scenario_generator *.py

lint3:
	pylint3 scenario_generator *.py

pep:
	pep8 ./scenario_generator/ ./*.py --max-line-length=120
