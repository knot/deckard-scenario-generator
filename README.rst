Automated test generating for Deckard - Scenario Generator
==========================================================

The Scenario Generator generates tests for Deckard based on browser DNS traffic. There are two parts. Traffic capture and Generating scenarios.

Content:
--------

- Traffic capture - set of scripts and files used for capturing DNS traffic
   - ``capture.py`` - main script for Traffic Capture
   - traffic-capture folder - contains all files needed by the main script
- Generating scenarios
   - ``sgen.py`` - script for generating scenarios
- scenario_generator folder - Folder containing python module with implementation of the capturing and generating.
- Makefile - Makefile for documentation, pylint3 and pep8
- configs folder - script configurations
- ThesisLatexSources - folder with latex sources for thesis paper (mostly obsolete)
- samples - Sample of domain name list (500 domains)
- doc_algorithm - Documentation of the algorithm behind generating scenarios

Capture:
--------

Captures DNS traffic of a web page on load. By default Chrome and Knot Resolver are used.
PCAPS can be stored in one directory or separated into several directories by argument tree (the structure being /browser/resolver/domain/).

.. code-block:: bash

   usage: CAP [-h] [-o DIR] [-b BR [BR ...]] [-r RS [RS ...]] [-d] [-t] [-n N] [-c FILE] [-v] [-l DIR]
            source [source ...]

   positional arguments:
     source                Source of domain name(s)

   optional arguments:
     -h, --help            show this help message and exit
     -o DIR, --output DIR  Existing folder to save the pcaps in
     -t, --tree            Store pcaps in directory tree hierarchy
     -b BR [BR ...], --browser BR [BR ...]
                           Browser to be used
     -r RS [RS ...], --resolver RS [RS ...]
                           Resolver to be used
     -d, --domains         Source is list of domains
     -n NUM, --number NUM  Number of retries
     -c FILE, --conf FILE  Use configuration file
     -v, --verbose         Verbose output
     -l DIR, --log DIR     Existing folder to save the logs in
     --threads NUM         Number of threads used (Default: number of processors * 5)
     -w SEC, --wait SEC    Seconds to wait in case of the Browser failing to start
     --retries NUM         Number of retries to start the Browser



Configuration:
~~~~~~~~~~~~~~

Configuration allows changing used software, number of repeats, verbosity, etc.

.. code-block:: bash

   [general]
   dir = ./traffic-capture # The traffic-capture directory
   output = ./data/pcaps
   logs = ./logs
   number = 1
   tree = no
   verbose = no

   [software]
   browsers = dig firefox chrome chrome-ios chrome-android # All suported browser versions + dig
   resolvers = bind unbound kresd # All superted resolvers


Scenario Generator:
-------------------

Generated scenarios from file or folder with source files.
Source files are PCAPS with DNS traffic with resolver's ip "127.0.0.1" or text file with queries on each line.
Uses only one name server as default.

.. code-block:: bash

   usage: SGEN [-h] [-l DIR] [-a] [-6] [-v] [-t] source [output]

   positional arguments:
     source             file or directory with files, PCAP or TXT with "dname rdtype" on each line format
     output             Output directory (stdout as default)

   optional arguments:
     -h, --help         show this help message and exit
     -l DIR, --log DIR  Logs directory
     -a, --all          Use all name servers per zone
     -6, --ipv6         Disable IPv6
     -v, --verbose      Verbose output, overwrites logging
     -t, --text         Source is a text file (dname rdtype)


Future extensions:
------------------

- configurable output hierarchy
- setup.py

Dependencies:
-------------

- Python 3 (Python >= 3.5.2 recomended)
   - progressbar2 >= 3.34.3
   - argparse >= 1.1
   - configparser >= 3.5.0
   - dpkt >= 1.9.1
   - dnspython > 1.15.0 (Requires version from github, next release after 14. 5. 2018)
   - pypcap >= 1.2.0
- Docker >= 17.12

Issues:
-------

- Chrome doesn't start without enough RAM. = Fixed by retries and --no-sandbox option.
- When over 100 scenarios are generated, the sgen is killed with SIGKILL

