\chapter{DNS}
\label{chap:DNS}

The DNS protocol serves as an address book for IP addresses. The typical use case is IP address mapped to a domain name. Domains are not only easier to remember than IPs, but might be based on, for example, name of a company and thus even easier to remember. A domain name is a sequence of labels from root to the full name. Each label is a node that can contain DNS records. The labels can be up to 63 and the whole domain name up to 255 characters long.  Besides IP addresses, the DNS protocol can contain other information such as a text string, certificate, ssh fingerprint, etc.

\section {Domain name space}
DNS is a huge database that consists of over 330 million domain names and each domain name has usually more than one DNS record. \cite{numberofdomainnames} This amount of records would be hard to maintain in one database and searching for records would be very inefficient. Therefore, the DNS content is distributed in a tree structure made of zones. The DNS tree is acyclic and consists of nodes and edges. A domain name is a path from the root to one of the nodes. The root itself is a specific label of zero lenght.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{Zones.pdf}
\caption{DNS Tree With Zones Example}
\end{figure}

The names of labels have to be unique for the path leading to that level. Two nodes cannot have the same value if they origin from the same parent node. 
\cite{rfc:2181} \cite{Matousek}

\section{DNS content distribution}

 A zone is a set of domains with various number of labels. A zone cut separates a parent zone, which contains the origin of a child zone, from the child zone. The child zone contains subdomains of the origin domain name in the parent zone. The presence of a zone cut can be discovered through a delegation from the parent zone redirecting subdomain names of the origin to the child zone. 
 
 The name servers provided by parent zone are authoritative for the child zone. At a zone cut name server redirect further. Name servers should provide an authoritative answer only for domain names in zones they contain. Each name server can contain one or more zones. \cite{rfc:2181}

First zone and the root of the DNS tree is the root zone managed by ICANN organization. The rest of the zones are distributed between multiple people and organizations. The number of top level domains (the first label from root) is limited and comprises national, generic and infrastructure domains.

There are different types of DNS servers. For the purpose of this thesis we will consider only two types based on function: an authoritative server and a resolver. If the resolver is asked to resolve a domain name, it asks the root server and expects an answer. The root server not knowing the answer delegates the resolver to a name server, which is authoritative for the first label of the domain name. The resolution continues by asking the server in the delegation until the resolver gets an answer. This process is called iterative resolution. Nevertheless, the resolver can also be set up to only forward the request to a different resolver. By forwarding the resolver is performing recursive query. \cite{Matousek}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.5]{ResolutionTypes.pdf}
\caption{Recursive vs iterative resolution \label{fig:resolution}}
\end{center}
\end{figure}

The authoritative name server manages one or more zones and responds with authoritative answer. The minimal number of name servers should be two, but usually more servers are used for bigger or frequently used zones.

\newpage
\section{Resource Records}

DNS has an extensive amount of different records that are used for storing DNS data. They are either stored in the zone files in a text form, or in a journal.  The records contain a name, type, class, TTL, record data length and record data. The most common record types are A for storing IPv4 addresses, PTR for reverse mapping of domain names, SOA, NS, and so on. Record data is specific for each record type. It can contain an IP address, a domain name, etc. The name is a domain name that identifies the node in which is the record stored. Class is one of DNS record classes. TTL defines for how long is the received record valid. Zero TTL means the response must be dropped after being used and not to be stored in a server cache.
\cite{Matousek}

Not all Resource Record types are relevant for creating scenarios from browser traffic. The common and relevant RR types for this thesis are following: A, AAAA, NS, CNAME, SOA, MX, DS, DNSKEY, RRSIG, NSEC, NSEC3 and NSEC3PARAM.



\subsubsection{A and AAAA}

A record stands for IPv4 address and  AAAA for IPv6 address. Both records make the most important part as they are possibly the only records browsers require. 

\begin{lstlisting}[caption={A and AAAA example}]
(name)			(TTL) (CLASS) (type) 	(RDATA)
nic.cz.			1800	IN		A   	217.31.205.50
nic.cz.			1800	IN		AAAA	2001:1488:0:3::2
\end{lstlisting}

\subsubsection{NS - Name Server}

NS record stores an authoritative name server for given domain name. This record is important for going through the DNS tree as the resolver is delegated until it finds the authoritative name server for the record of the domain name it is looking for.

\begin{lstlisting}[caption={NS example}]
nic.cz.			1800	IN	NS	a.ns.nic.cz.
nic.cz.			1800	IN	NS	d.ns.nic.cz.
nic.cz.			1800	IN	NS	b.ns.nic.cz.
\end{lstlisting}

\subsubsection{CNAME - Canonical Name}

The record data in a CNAME resource record is a domain name. CNAME record redirects from one domain name to another. They are usually accompanied by A or AAAA record. 

\begin{lstlisting}[caption={CNAME example}]
gitlab.labs.nic.cz.	30	IN	CNAME	git.nic.cz.
git.nic.cz.		30	IN	A	217.31.192.18
\end{lstlisting}

\newpage

\subsubsection{MX - Mail Exchanger}

MX record holds a domain name of a mail server for a given domain name. MX contains also a preference number, first in the RDATA. The server with the lowest number is used. Other servers are used when the server with lower priority number is unavailable. For getting the mail server for an e-mail address, for example, filip.siroky@nic.cz we need the MX record for nic.cz.

\begin{lstlisting}[caption={MX example}]
nic.cz.			1800	IN	MX	10 mail.nic.cz.
nic.cz.			1800	IN	MX	20 mx.nic.cz.
nic.cz.			1800	IN	MX	30 bh.nic.cz.
\end{lstlisting}

\subsubsection{SOA - Start of Authority}

SOA record contains information about records located in given a zone. Each zone has only one SOA record with the following records:

\begin{description}

\item [MNAME] - primary authoritative name server

\item [RNAME] - an e-mail address of a person responsible for the zone, but instead of '@', there is a dot in the name.

\item[SERIAL] - Number identifying the version of the zone. It should be changed every time the zone is modified. Sometimes a timestamp is used, but recommended is YYYYMMDD format of the changes date followed by two-digit ID number.

\item[REFRESH] - Time interval in seconds to wait before the secondary server should refresh the zone. 

\item[RETRY] - If the zone transfer failed, the secondary server will try again in RETRY number of seconds.

\item[EXPIRE] - The time in seconds for how long are the records valid. After this period of time the zone is no longer authoritative.

\item[MINIMUM] - States the TTL for records in the zone.

\end{description}

\subsubsection{DS, DNSKEY, RRSIG, NSEC, NSEC3 and NSEC3PARAM}
DNSSEC related records are important for crucial part of resolvers task. DNSSEC is important security extension and is further described in section \ref{sec:DNSSEC}.

\newpage

\section{DNS message format}

The DNS message format consists of 5 sections as shown in listing \ref{message:format}. Header contains information about the content in the other sections, type of a query and whether the message is response or query, etc. Question section contains query type (QTYPE), query class (QCLASS) and query name (QNAME). The sections answer, authority and additional can be empty. If section answer isn't empty, it contains answer RRs to the question. Authority section contains RRs delegating to an authoritative nameserver. Last but not least, Additional section contains RRs connected to the answer, but not strictly answer to the question.
\lstset{keepspaces=true}
\begin{lstlisting}[caption={DNS message format}, label={message:format}]
	+-----------------------+
	|	Header		|
	+-----------------------+
	|	Question	| the question for the name server
	+-----------------------+
	|	Answer		| RRs answering the question
	+-----------------------+
	|	Authority	| RRs pointing toward an authority
	+-----------------------+
	|	Additional	| RRs holding additional information
	+-----------------------+
\end{lstlisting}

\subsection{DNS message Header}
\begin{lstlisting}[caption={DNS message Header format}, label={message:header}]
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      ID                       |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    QDCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ANCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    NSCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ARCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
\end{lstlisting}
\lstset{keepspaces=false}
\begin{description}
\item [ID] - Identifier copied into answer for message matching.
\item [QR] - Flag specifying the message is query (0) or answer (1).
\item [OPCODE] is 4 bits from flags specifying the message kind. Defined kind are Query (0), IQuery (1, now obsolete), Status (2), Notify (4), and Update (5).
\item [AA] - Authoritative Answer is response specific flag indicating the answer comes from authoritative server for the domain name in the question section.
\item [TC] - Truncation specifies the message was truncated, because it was too large for the transmission channel.
\item [RD] - Recursion Desired signals the server to resolve the query recursively. Not every server supports recursive resolution as it is optional.
\item [RA] - Recursion Available is set in the response if the name server supports recursive resolution.
\item [Z] is Reserved and must be zero.
\item [RCODE] - Response code states the status of the response. For example, No Error (0), Format Error (1), Server Failure (2), etc.
\item [QDCOUNT, ANCOUNT, NSCOUNT, ARCOUNT] specify the number of resource records in the given section.
\end{description}

\cite{rfc:1035} \cite{DNSparameters}

\section{EDNS(0)}

Extension Mechanisms for DNS are backward-compatible mechanisms allowing DNS protocol to grow without the need to scrap the current infrastructure and the whole protocol. Without these extensions the maximum message size over UDP is 512 bytes which is very limiting when sending IPv6 addresses, RRSIGs, additional records, etc.

The use of EDNS is negotiated between each pair of hosts in DNS resolution. For example, between DNS resolver and an authoritative server. It allows for bigger UDP DNS messages, more flags and return codes. For example, DNSSEC uses additional flag DO to request DNSSEC data needed for validation of the record in the response. The extended packet size allows for avoiding the extensive use of TCP for DNS.

EDNS introduces new resource record OPT that allows additional RCODE and flags. The record is put into the additional section of the query. The OPT record must be in the response if the extension has been accepted. The OPT RR  is not cached, nor forwarded. Only one OPT RR can be present.

The OPT RR has the same format as other RRs, where domain name of the record is always root (0). Type is the OPT (41). The class is replaced by requestor's UDP payload size. Instead of TTL, OPT RR has the extended RCODE,  flags and EDNS version. RDATA is a variable set of option pairs in form of attribute and value. The options have option code, length and data. The length depends on the data and data depends on the code. The different options don't have a given order.
\cite{rfc:6891}

\newpage

\section{DNSSEC}
 \label{sec:DNSSEC}

Domain Name System Security Extensions allows for DNS data origin authentication and integrity protection. Also public key distribution is possible. However, DNSSEC doesn't confidentiality.

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{DNSSEC-signatures.png}
\caption{Signing and validating A record \protect\cite{Matousek}}
\label{DNSSEC-signatures}
\end{figure}

DNSSEC uses asymmetric cryptography thus sets of public and private keys.  Private key is used for signing records and public is used for validation of the signatures. It is based on authentication chain (chain of trust), which is sequence of DS and DNSKEY records from root zone to the final zone. DS record starting with root DS, validates DNSKEY in a child zone. The validated key is Key Signing Key and this key validates Zone Signing Key. More details about KSK and ZSK is in subsection \ref{DNSKEY}. ZSK validates DS record, and the process continues. We use public KSK for validating public ZSK to ensure we didn't receive falsified records, signatures and keys. The process of establishing trust chain is depicted in figure \ref{DNSSEC-chain}. Public ZSK then verifies RRSIGs of individual records as shown in figure \ref{DNSSEC-signatures}. The signatures contain signed hash of the given record.


\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{DNSSEC-chain.png}
\caption{DNSSEC chain of trust \protect\cite{Matousek}}
\label{DNSSEC-chain}
\end{figure}


If there is missing DNSSEC delegation (missing DS record), the chain of trust is broken and child zones that are signed form islands of security. These zones shouldn't be taken as secured, but we can establish new trust anchor in our resolvers configuration.

\subsection{DNSKEY - DNS Key Record}
\label{DNSKEY}

DNSKEY contains public key of KSK or ZSK in it's record data along with flags, protocol and algorithm. Seventh bit of flags with value one means the key is a zone signing key. Bit 15 is the Secure Entry Point. The rest of the flags are reserved. ZSK have flags value 256 and KSK 257. Protocol number if a constant. It must have value of 3. The algorithm field contains identification of the algorithm used for creating the key. In listing \ref{NSEC3:zone} are KSK and ZSK examples used for signing the zone in the listing.

%\begin{lstlisting}[caption={DNSKEY KSK and ZSK example}]
%nic.cz.			432	IN	DNSKEY	257 3 13 (
%			LM4zvjUgZi2XZKsYooDE0HFYGfWp242fKB+O8sLsuox8S6MJTowY8lBD 
%			 jZD7JKbmaNot3+1H8zU9TrDzWmmHwQ== )
%nic.cz.			432	IN	DNSKEY	256 3 13 (
%			TfeQ6gwLVX2Bbi9Fe5N3VnNv663Tsb8Vogcj1IEtYsI4OUNjNEUmUIrd
%			 io/ENj96yPceVqOBmIVWXtQlTXzUIQ== )
%\end{lstlisting}

\subsection{RRSIG - Resource Record Signature}
RRSIG contains signed hash of its own RDATA and resource record set, the records with the same type, class, and TTL. The RRSIG's RDATA consists of the following fields:
\begin{description}
\item [Type covered] The type of the signed record, algorithm number that was used to create the signature.
\item  [labels] This field states the number of labels in original RRSIG RR owner name to determine the owners name used in generating the signature in case of the RRSIG being created from a wildcard.
\item [Original TTL] The TTL of the covered RRset is used to reconstruct the original TTL when the resolver is validating the signature while the records are in cache.
\item [Signature Expiration and Inception] The validity of a signature is defined by these two fields. The signature must not be used for validation, if the time of validation is not in the specified time interval. Both times are represented by Unix Time, the number of seconds since 1 January 1970 00:00:00 UTC. 
\item [Key Tag] Key Tag identifies the signature that created this RRSIG and is calculated out of the key.
\item [Signers Name] The value identifies the owner name of the DNSKEY to be used for validation, thus it must be the zones name.
\item [Signature] The signature contains signed RRSIG RDATA without the signature it self and other RRs. 
\end{description}

An example of RRSIG is in listing \ref{NSEC3:zone}.

%\begin{lstlisting}[caption={RRSIG example}]
%nic.cz.			265	IN	RRSIG	DNSKEY 13 2 1800 20180516055257 20180502214002 (
%			57996 nic.cz. NRX0HxC8JqJ6abvJ7dF4JnYG7n4uklCCx+Pb
%			EIu1heRRt9TH0DHbHG1b TjHi53IYrXoKL+ONR+G3mvUEZUjEMw== )
%nic.cz.			265	IN	RRSIG	DNSKEY 13 2 1800 20180516162002 20180502214002 (
%			61281 nic.cz. uAB+29/Pgbtt6STPlP4MkX0VmVYhpy/vmpU9
%			bjbPiErgE1V3KLo6whoy 1IiIJgHGjsY7AdduZMDKDQugtqaTmA== )
%nic.cz.			265	IN	RRSIG	A 13 2 1800 20180516020339 20180502214002 (
% 			57996 nic.cz. 0Hsx0CNnHdUQxdkYZfMs7XyMW7sgAcEu
% 			porfBBM7mvb6oJQWM1ygSCPk awO20uA64ibst5U+mIK26d/5n9Ehnw== )
%\end{lstlisting}

\subsection{DS - Delegation Signer}

DS contains Key Tag computed from the DNSKEY, Algorithm of the DNSKEY, Digest Type stating the algorithm used to calculate the digest, DNSKEY digest of the child zone KSK DNSKEY record to validate it. DS and DNSKEY have the same owner domain name. The digest is calculated from owner name and DNSKEY RDATA.

\begin{lstlisting}[caption={DS example}]
nic.cz.			3600	IN	DS	61281 13 2 ( 4104D40C8FE2030BF7A09A199FCF37B36F7EC8DDD
				16F5A84F2E61C24 8D3AFD0F )
\end{lstlisting}

\subsection{NSEC - Next-Secure Record}
NSEC record contains next domain name that contains authoritative records in a zone and types of the records the next domain name has. If the name is the last, then it points to the first one. NSEC records are used as an authoritative proof of nonexistence of a record in the zone. The proof lies in pointing to the next domain name saying no more records are between current domain name and the next one and signing the proof by DNSSEC. It is used to prevent an attacker to send a forged record that actually doesn't exist. NSEC and RRSIG exist even for CNAME records.

The order in which the names form a chain is called Canonical DNS Name
Order. First, domain names are ordered by their rightmost (the most significant) label. If the labels are identical, the names are compared by the second most significant and continue in such manner until the names are ordered. See listing \ref{ordered:names} for example. The Canonical RR form requires the domain names must not be compressed in any way, all uppercase characters must be replaced by their lowercase equivalent, wildcards stay unexpanded, and the RR's TTL is of the original value as the value of RRSIG's Original TTL. 

\begin{lstlisting}[caption={Canonical DNS Name
Order example}, label={ordered:names}]
example
a.example
yljkjljk.a.example
Z.a.example
zABC.a.EXAMPLE
z.example
\001.z.example
*.z.example
\200.z.example
\end{lstlisting}

In listing \ref{NSEC} we can see \texttt{fit.vutbr.cz.} NSEC record pointing to \texttt{kerberos.fit.vutbr.cz.} and the records \texttt{fit.vutbr.cz.} contains, and the signature of the NSEC record.

\begin{lstlisting}[caption={NSEC example}, label={NSEC}]
fit.vutbr.cz. 86398 IN NSEC kerberos.fit.vutbr.cz. NS SOA MX TXT RRSIG NSEC DNSKEY
fit.vutbr.cz. 86398 IN RRSIG NSEC 5 3 86400 20111202094352 (
20111102094352 35421 fit.vutbr.cz.
nmUzpEnG4vIZN0uVRo5ja/cSEZG0Jec8fMPLAAiU2c2X
1PmpGYjLB6AFNCTEYM9+ZnEAACWY7xsYEFWxYoc= )
\end{lstlisting}

\newpage

\subsection{NSEC3 and NSEC3PARAM}

Problem with NSEC is the ability to get the whole content of a zone. Even though the data is public, getting all the information without any limitation might not be desirable. To counter this issue, NSEC3 was created. NSEC3 has the same function as NSEC to prove the nonexistence of a domain name. However, while having a chain of domain names in canonical, NSEC3 hides the actual name by hash function. The order remains the same. An example of hashed domain names is in listing \ref{NSEC3:names}.

\begin{lstlisting}[caption={NSEC3 Canpnical Order example\protect\cite{Matousek}}, label={NSEC3:names}]
H(example) = 0p9mhaveqvm6t7vbl5lop2u3t2rp3tom
H(a.example) = 35mthgpgcu1qg68fab165klnsnk3dpvl
H(ai.example) = gjeqe526plbf1g8mklp59enfd789njgi
H(ns1.example) = 2t7b4g4vsa5smi47k61mv5bv1a22bojr
H(w.example) = k8udemvp1j2f7eg6jebps17vp3n8i58h
H(*.w.example) = r53bq7cc2uvmubfu5ocmm6pers9tk9en
H(y.w.example) = ji6neoaepv8b5o6k4ev33abha8ht9fgc
\end{lstlisting}

The NSEC3 record contains information about the used algorithm, flags, salt (an initialization value) and its length, the number of hashing iterations, next hashed domain name and its length and bitmap with a list of DNS types the domain name has. The only flag for NSEC3 so far is OPT-OUT and it means that the NSEC3 record covers zero or more unsigned delegations. An example of NSEC3 record is in listing \ref{NSEC3:zone}.

NSEC3PARAM also contains hash algorithm, flags, iterations, salt length and salt it self. This fields except flags have the same value as those in NSEC3 record if the NSEC3PARAM flags are 0. It is used by authoritative servers to calculate the hashed owner names. An example of NSEC3PARAM is in listing \ref{NSEC3:zone}
\cite{rfc:5155}

\subsection{DNSSEC signed zone example with NSEC3}

In listing \ref{NSEC3:zone} is an example of signed minimalistic zone. The original zone without signatures contained 4 records. The original records SOA, A record of the origin, NS and A record of the NS are marked red. After signing with DNSSEC and NSEC3 the zone has 17 records. 2 NSEC3 records, 1 NSEC3PARAM, 2 DNSKEYs, original 4 records and 8 signatures.


\cite{rfc:3658} \cite{rfc:4034} \cite{rfc:3757} \cite{Matousek}

\newpage

\begin{lstlisting}[caption={DNSSEC with NSEC3 zone example}, label={NSEC3:zone}]
;; Zone dump (Knot DNS 2.6.4)
<@\textcolor{red}{example.            	3600	SOA ns1.example. mtain.example. 20180512 3600 900 360000 3600}@>
<@\textcolor{red}{example.            	3600	A	1.2.3.4}@>
<@\textcolor{red}{example.            	3600	NS	ns1.example.}@>
example.            	3600	DNSKEY	256 3 13 ( W4ejK4GJ9HZAQveVgt3UkzIDqTXL/MfVVzkdLtTIthpJ/qXS+v1
				/al93SXS1pVPKuitrwiGqID0f7kXsqIs7aA== )
example.            	3600	DNSKEY	257 3 13 vKwHBeYkE/CqFkBA/AEGTj5BlhXcqMDrEmB759JsTzvcpOWRE33
				otIzQtRZQ1Fgu4j/7UL6AcBp7QkxqoO6dDw== )
example.            	0	NSEC3PARAM	1 0 10 24EA176025E0B8DD
<@\textcolor{red}{ns1.example.        	3600	A	1.2.3.5}@>
;; DNSSEC signatures
example.            	3600	RRSIG	A 13 1 3600 20180524070946 20180510053946 17561 example. (
			 DiXfv9G9OMRecT02iN20Vbxm1vWt0UZ+dr+MklGIb3TVtlJEtA6QUryE2
			 N6ZJa2qE2NYQXsWvioKZGUNbN6ffQ== )
example.            	3600	RRSIG	NS 13 1 3600 20180524070946 20180510053946 17561 example. (
			dKX/6yeBXY3nohBwakeObE7MviS3HSnime8wfJA/qCjGTk7NBGg2CHD8rMT
			/630s09TN0J0A+QRjhfDOCJr+yQ== )
example.            	3600	RRSIG	SOA 13 1 3600 20180524071606 20180510054606 17561 example. ( 
			zOqmE3FRfMj/RmIOkHqD36+lNgjP90hJNDity1S9EXSkKNTwqUd22/QdO9f
			v64G/qE3FUK95Kd3mr7cQZWr/IA== )
example.            	3600	RRSIG	DNSKEY 13 1 3600 20180524070946 20180510053946 35712 example. (
			srH6Cjb60XfuKXdwDrrpQwzpfmjtBBXn3qPesYnmJroirk0SsbXOZ564YPV
			+xzitf263BIVk+ezbkHvuurjkIQ== )
example.            	0	RRSIG	NSEC3PARAM 13 1 0 20180524070946 20180510053946 17561 example. ( 
			DdI9FAoIzq2P/VgiR6O657T9pOhaE3Sp3BcckD46QOyi44xNzyPMp3bVXrn8
			zUkjgFSmzcXq8W3QhfXvDUGK9A== )
ns1.example.        	3600	RRSIG	A 13 2 3600 20180524070946 20180510053946 17561 example. (
			PiHTOqN5MaS9AjZbFBsxHxlOnXM4ufnhCAl1KEsCpczjqWIKh5AnW4+mrVAhq
			xWheb7e006vl1Imq0xCzuWPlg== )
;; DNSSEC NSEC3 chain
d85eieav0rbkeepl5qsmcs8dr7ig76qn.example. 3600	NSEC3	1 0 10 (
			 24EA176025E0B8DD TGC83JPIF6EA57D8UHJD5OM32LJG4O1U
			 A NS SOA RRSIG DNSKEY NSEC3PARAM )
tgc83jpif6ea57d8uhjd5om32ljg4o1u.example. 3600	NSEC3	1 0 10 (
			 24EA176025E0B8DD D85EIEAV0RBKEEPL5QSMCS8DR7IG76QN
			 A RRSIG )
;; DNSSEC NSEC3 signatures
d85eieav0rbkeepl5qsmcs8dr7ig76qn.example. 3600	RRSIG	NSEC3 13 2 3600 (
			20180524071606 20180510054606 17561 example.
			Mdmnobrybtx8O6qxCkJK3/HDvX3Cy9GJ7CRtwxTCXfX5bKRd10
			g56bhC0IEslnFon+YR8ld1oidCE3ngdfCkdA== )
tgc83jpif6ea57d8uhjd5om32ljg4o1u.example. 3600	RRSIG	NSEC3 13 2 3600 (
			20180524070946 20180510053946 17561 example.
			0fa8PFyf2Ci+fvmSrO16LHa2yoW5NYMm/ymJ9DENShH+a9sXe18
			1a0D6ZjA7YTkijx5w9u4wOo8SxldT+cM2ag== )
;; Written 17 records
;; Time 2018-05-10 09:16:06 CEST
\end{lstlisting}

\section{Query Minimization}

Query minimization impacts the resolvers behavior significantly. For scenario to work with both standard resolution and query minimization, the behavior difference has to be addressed.

 With standard resolution, searched domain is always present in the queries. Query name minimization the query name contains only one more label than the server which is being queried is authoritative for. This behavior starts with root server. That server is queried for top level domain with NS query type (\texttt{com.}). One server from the response is queried for the domain with second-level domain (\texttt{example.com.}). This continues until the resolver found authoritative server for the searched domain. 
 
If two labels aren't separated by a zone cut the resolver will receive, for example, NODATA for the first label or an authoritative answer with a delegation. In that case resolver adds the next label and retries the query.

The RFC describes possible resolution behavior to avoid failing because of some broken implementations of name servers that do not respond to NS query. Instead of querying \texttt{NS example.com} the resolver should try to query \texttt{A\_.example.com} and expect and referral. Also, if nonterminal subdomain is empty, some servers return NXDOMAIN. This shouldn't stop the resolution and next label should be added. \cite{rfc:7816}