\chapter{Used tools}
\label{chap:UsedTools}

Automated testing developed in this thesis uses several tools to achieve its goals. Tools Docker, tcpdump and Selenium are used to capture DNS traffic. Deckard is the tool we create scenarios for and test the resolver with. Respdiff is a tool showing differences in different resolver implementations. This chapter describes these tools in detail.

\section{Selenium}
Selenium is a tool for automating browser testing. Selenium supports many browsers from which Chrome and Firefox are the most important as they cover over half of the browser market and are available for Linux as well. Furthermore, Chrome also allows mobile browser version simulation. If loading a page is not sufficient, it is easy to extend the script for going through the page to get more of its DNS traffic.
\cite{selenium}

\section{Docker}

Docker is software container platform useful for creating environment fit for capturing DNS traffic with tcpdump inside. A separated network interface allows easy filtering the required traffic. Docker also comes with Selenium images making Selenium easy to use. \cite{docker}

\section{Deckard}

Deckard is a tool made and used by CZ.NIC labs for testing a DNS software in a controlled and stable environment. Deckard is designed to ensure reproducible tests for DNS resolvers. At the time of writing this document, Deckard supports Knot Resolver, Unbound, and PowerDNS.

Deckard tests the resolver by playing a scenario (More about scenarios in subsection \ref{deckard:scenario}). To achieve deterministic behavior, Deckard requires all responses, the resolver might ask, to be in the scenario. If anything is missing, or the resolvers answer doesn't match the expected answer, the test will fail.

Deckard provides a way to simulate changing a records, so called ranges. Deckard also supports generic answers to multiple queries by using different match values and adjusting answers from the server to fit the ID, question, etc. This comes in handy, especially when we need a delegation of a domain with many subdomains. So, instead of having multiple entries, each having the same delegation, you get only one generic.

\subsection{Scenario}
\label{deckard:scenario}

Scenarios comprise three parts - config,steps and ranges. Further details can be found in \texttt{README.rst} in the Deckard's Gitlab repository \cite{deckard}. Steps are a sequence of query and answer. The query is a question Deckard asks the resolver. The answer is the response Deckard expects. The following examples describe steps in detail.

\subsubsection{Config}

Configuration affects behavior of binary under test. The options are, for example, trust-anchor, stub-addr, val-override-date, etc.

\subsubsection{Steps}
Each steps has Id to indicate the order in which they are processed. This Id also affects ranges as is described in subsubsection \ref{sec:Ranges}. Then there are four kinds of steps. Step \texttt{QUERY}
defines that following entry contains a query that is an question Deckard should send to the tested resolver. Step \texttt{CHECK\_ANSWER} describes the following entry is an answer Deckard should receive from the resolver. For the purpose of this thesis steps \texttt{TIME\_PASSED ELAPSE} and \texttt{REPLY} are not important. \texttt{TIME\_PASSED ELAPSE} moves a faked system time for binary being tested, and reply defines reply to query of binary. Step examples can be found in listings \ref{code:Steps:query} and \ref{code:Steps:checkanswer}.

\begin{description}
\item [Entry blocks] contain several fields. Reply field represents flags and RCODE of the message in a text format and should be present in every entry. Section question, answer, authority and additional are followed by a text form of resource record sets and can be omitted if the section is empty. Mentioned fields are common for all occurrences of entries in the scenario.

\item [Match field] is used to determine, if answers have the same values of certain DNS message values. It is used for comparing expected and received answer from the resolver, for example in listing \ref{code:Steps:checkanswer} Deckard would compare the response the resolver sent him for query in \ref{code:Steps:query} and all fields of DNS message would have to match for the scenario to pass. The elements that have to match can be parts of the query or, for example, a subdomain (instead of one query name, subdomains to that query are accepted as well), all (the whole query must match), etc.

\item [Field Adjust] is specific for entries in ranges and states witch elements should be adjusted before answering. There are two options. Either question name and Id are copied from the request to the response, or the whole question section is copied.

\end{description}
\lstset{
   showlines=true
}
 \begin{minipage}[t]{.45\textwidth}
\begin{lstlisting}[label={code:Steps:query}, caption={STEP QUERY}]
; send query to the resolver
STEP 1 QUERY
ENTRY_BEGIN ; query to be send
REPLY RD ; <OPCODE RCODE flags>
SECTION QUESTION
www.nic.cz IN A
ENTRY_END






\end{lstlisting}
\end{minipage}
\begin{minipage}[t]{.45\textwidth}
\begin{lstlisting}[label={code:Steps:checkanswer}, caption={STEP CHECK\_ANSER}]
; compare the answers
STEP 2 CHECK_ANSWER
ENTRY_BEGIN
MATCH all ; all fields must match
REPLY QR RD RA NOERROR
SECTION QUESTION
www.nic.cz. IN A
SECTION ANSWER
www.nic.cz. IN CNAME deckard.example.cz.
deckard.example.cz. IN A 1.2.3.4
SECTION AUTHORITY
SECTION ADDITIONAL
ENTRY_END
\end{lstlisting}
\end{minipage}
\lstset{
   showlines=false
}

\subsubsection{Ranges}
\label{sec:Ranges}

Ranges are list of all responses the server they represent contains. They are list of DNS messages in a different format. Each range has one or more IP addresses. When the tested resolver tries to query a DNS server, Deckard intercepts this query and looks for a range with the IP address of the server. If Deckard finds it, it looks for response valid for the request - elements listed in the match field meet the expectations. Range also contains minimal and maximal ID. If a step ID is out of the scope of the range, than Deckard doesn't look for a response to the resolver in that range.
Listing \ref{code:Range} shows an example of a range with one response.

\begin{lstlisting}[label={code:Range}, caption={Range example}]
; Range with two IPs and range from 0 to 1000
RANGE_BEGIN 0 1000 
	ADDRESS 216.239.34.10
	ADDRESS 2001:4860:4802:32::a

ENTRY_BEGIN
MATCH qname  qtype
ADJUST copy_id
REPLY QR AA RD NOERROR
SECTION QUESTION
www.nic.cz IN A
SECTION ANSWER
www.nic.cz IN A 1.2.3.4
ENTRY_END

RANGE_END
\end{lstlisting}

\subsubsection{Entry matching and query adjusting}

Here is an example of matching entries in Deckard and adjusting the DNS message. In listing \ref{code:question} is a DNS question the resolver sends to a server. However, Deckard didn't find exact match entry. Instead, Deckard found entry requiring subdomain to match. The entry is in listing \ref{code:entry}. The values fulfilling the match condition are marked blue in both listing. As you can see, \texttt{www.nic.cz.} is a subdomain of \texttt{nic.cz.} and therefore the condition is met.

In listing \ref{code:answer} is DNS response message based on the entry in listing \ref{code:entry}. The message is adjusted to fit the request based on the \texttt{ADJUST} field value. The final message is in listing \ref{code:entry:answer}. Adjusted fields are marked red on both listings. 

 \begin{minipage}{.45\textwidth}
\begin{lstlisting}[label={code:question}, caption={DNS Question}]
id 19759
opcode QUERY
rcode NOERROR
flags RD
;QUESTION
www.<@\textcolor{blue}{nic.cz.}@> IN A
;ANSWER
;AUTHORITY
;ADDITIONAL
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[label={code:entry}, caption={Entry in Deckard's range}]
ENTRY_BEGIN
MATCH subdomain
; copy_id copies ID and query name
ADJUST copy_id 
REPLY QR AA RD NOERROR
SECTION QUESTION
<@\textcolor{blue}{nic.cz.}@> IN NS
SECTION AUTHORITY
nic.cz. IN NS ns1.nic.cz
ENTRY_END
\end{lstlisting}
\end{minipage}

 \begin{minipage}{.45\textwidth}
 \begin{lstlisting}[label={code:answer}, caption={DNS Answer}]
<@\textcolor{red}{id 19759}@>
opcode QUERY
rcode NOERROR
flags QR RD RA
;QUESTION
<@\textcolor{red}{www.nic.cz. IN A}@>
;ANSWER
;AUTHORITY
nic.cz.  IN NS ns1.nic.cz
;ADDITIONAL
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[label={code:entry:answer}, caption={DNS Entry Answer}]
<@\textcolor{red}{id 26170}@>
opcode QUERY
rcode NOERROR
flags QR RD RA
;QUESTION
<@\textcolor{red}{nic.cz. IN NS}@>
;ANSWER
;AUTHORITY
nic.cz.  IN NS ns1.nic.cz
;ADDITIONAL
\end{lstlisting}
\end{minipage}
\newpage
\subsection{Deckard's network}

The figure \ref{deckard:comunication} depicts communication between Deckard and the tested resolver which is described below.

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{DeckardComunicationV2.pdf}
\caption{One Deckard step communication}
\label{deckard:comunication}
\end{figure}

First, Deckard runs the resolver under test as subprocess in an isolated network environment using modified socket wrapper. Then, Deckard sends the first query defined in steps from the scenario to the tested resolver. When the resolver attempts to communicate with any server, Deckard intercepts the communication and looks for a valid response in scenarios ranges. If we find a matching answer, the query is returned. If no answer matches Deckard responds with \texttt{SERVFAIL}.

When resolver comes up with an answer, Deckard compares it to the next step. If the response doesn't match the step, the test failed, otherwise Deckard proceeds with the next pair of steps. If all steps are successful and Deckard answered all queries, the test passed. \cite{deckard}

\newpage

\section{Respdiff v2}

Respdiff v2 is a set of six tools for fast and easy DNS response comparison. It allows creating reports and see differences between DNS resolver implementations and their versions on large number of queries. All tools use one folder with Lightning Memory-Mapped Database (LMDB) environment.

To analyze the differences between servers, respdiff requires us to set up the servers and put their information, such as IP address and port they listen on, protocol for the communication and restart script, in the configuration file. One of the servers has to be a target. The target should be the server whose behavior we want to analyze. The rest of the servers are referred to as others. 

The differences might be, for example, AD flag present in a response from the target and not present in responses from the others. Also, the server might return SERVFAIL, NXDOMAIN, or other RCODE instead of NOERROR. \cite{respdiff}

The respdiff tools are following:

\begin{description}
\item [Qprep] creates queries in a wire format and saves them in the new LMDB environment it created. Source of the queries is standard input in form of query name and type. All queries have set EDNS buffer size to 4096 B and DO flag for test input. Another source accepted is a PCAP file containing DNS packets with QR bit 0 on port 53. 

\item [Orchestrator] takes queries from the LMDB database, sends them to all configured servers and stores their answers in the database.

\item [Msgdiff] compares the responses and stores the differences of configured fields. 

\item [Diffrepro] is an optional tool for filtering out differences that weren't reproducible over time or when the upstream answer differed between resolvers.

\item [Diffsum] creates a final report from the differences created by \textbf{Msgdiff}. If others don't agree, the query is skipped. If others agree, but target doesn't, the difference is reflected in the report.

\end{description}

