#!/usr/bin/env python3
""" DNS Traffic Capture
    ===================

    Captures DNS traffic of a web page on load. By default Chrome and Knot Resolver are used.

    PCAPS can be stored in one directory or separated into several directories by argument tree
    (the structure being /browser/resolver/domain/).

    >>> ./capture.py -h
    usage: CAP [-h] [-o DIR] [-b BR [BR ...]] [-r RS [RS ...]] [-d] [-t] [-n N] [-c FILE] [-v] [-l DIR]
           source [source ...]
    positional arguments:
        source                Source of domain name(s)
    optional arguments:
        -h, --help            show this help message and exit
        -o DIR, --output DIR  Existing folder to save the pcaps in
        -t, --tree            Store pcaps in directory tree hierarchy
        -b BR [BR ...], --browser BR [BR ...]
                           Browser to be used
        -r RS [RS ...], --resolver RS [RS ...]
                           Resolver to be used
        -d, --domains         Source is list of domains
     -n NUM, --number NUM  Number of retries
     -c FILE, --conf FILE  Use configuration file
     -v, --verbose         Verbose output
     -l DIR, --log DIR     Existing folder to save the logs in
     --threads NUM         Number of threads used (Default: number of processors * 5)
     -w SEC, --wait SEC    Seconds to wait in case of the Browser failing to start
     --retries NUM         Number of retries to start the Browser
"""
import argparse
from scenario_generator.capture import Capture


def process_argv():
    """ Process program arguments

    @return: Parsed arguments
    @rtype: namespace
    """
    parser = argparse.ArgumentParser(prog='CAP')
    # Input/Output
    parser.add_argument("source", nargs='+', help="Source of domain name(s)")
    parser.add_argument("-o", "--output", metavar="DIR", help="Existing folder to save the pcaps in")
    parser.add_argument("-t", "--tree", action="store_true", help="Store pcaps in directory tree hierarchy")
    # Content
    parser.add_argument("-b", "--browser", nargs="+", metavar="BR", help="Browser to be used")
    parser.add_argument("-r", "--resolver", nargs="+", metavar="RS", help="Resolver to be used")
    parser.add_argument("-d", "--domains", action="store_true", help="Source is list of domains")
    parser.add_argument("-n", "--number", metavar="NUM", type=int, help="Number of retries")
    # Settings
    parser.add_argument("-c", "--conf", metavar="FILE", help="Use configuration file")
    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose output")
    parser.add_argument("-l", "--log", metavar="DIR", help="Existing folder to save the logs in")
    # Parallelization
    parser.add_argument("--threads", metavar="NUM", help="Number of threads used (Default: number of processors * 5)")
    parser.add_argument("-w", "--wait", metavar="SEC", help="Seconds to wait in case of the Browser failure")
    parser.add_argument("--retries", metavar="NUM", help="Number of retries to start the Browser")
    args = parser.parse_args()
    return args


def main():
    """ Capture main"""
    args = process_argv()
    cap = Capture()
    cap.apply_args(args)
    ret = cap.prepare()
    if ret:
        return ret
    ret = cap.build_docker()
    if ret:
        return ret
    ret = cap.run_input()
    if ret and cap.failed:
        print(len(cap.failed), " captures failed. Rerunning.")
        ret = cap.run_failed()
        return ret
    return ret


if __name__ == "__main__":
    main()
