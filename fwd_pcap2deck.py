#!/usr/bin/python3

import ipaddress
from datetime import datetime
import pathlib
import sys
from typing import Dict, Iterator, List, Optional, Tuple, Union

import dpkt.pcap
import dns.message

import scenario_generator.query

IP = Union[ipaddress.IPv4Address, ipaddress.IPv6Address]

LINK_TYPES = {
    dpkt.pcap.DLT_EN10MB: dpkt.ethernet.Ethernet,
    dpkt.pcap.DLT_LINUX_SLL: dpkt.sll.SLL,
}

# TODO: this should be proper generator ... and also return metatada like
# timestamp for libfaketime, expected trust anchor etc.
def process_pcap(
            filename_in: str,
        ) -> None:
    with open(filename_in, 'rb') as fin:
        pcap_in = dpkt.pcap.Reader(fin)
        if pcap_in.datalink() not in LINK_TYPES:
            logging.critical("Unsupported PCAP linktype")
            sys.exit(1)

        # read filter for 53/udp
        pcap_in.setfilter('udp port 53')

        parse = LINK_TYPES[pcap_in.datalink()]

        for ts, pkt in pcap_in:
            link = parse(pkt)

            ip = link.data
            if not isinstance(ip, (dpkt.ip.IP, dpkt.ip6.IP6)):
                continue

            udp = ip.data  # NOTE: ip packet musn't be fragmented
            if not isinstance(udp, dpkt.udp.UDP):
                continue

            payload = udp.data
            if not isinstance(payload, bytes):
                continue

            if len(payload) < 3:
                continue  # small garbage isn't supported
            if payload[2] & 0x80 == 0:
                continue  # QR=1 -> response

            msg = dns.message.from_wire(payload)

            deckard_query = scenario_generator.query.Query(msg)
            print(deckard_query.to_string(server='127.0.0.1'))

if len(sys.argv) != 2:
    print(f"Usage: {sys.argv[0]} <pcap>")
    print("Transform DNS answers from PCAP into Deckard's RANGE entries")
    print("Do not forget to add CONFIG/SCENARIO/STEP pieces by hand")
    sys.exit(1)

pcap_path = sys.argv[1]
mtime = pathlib.Path(pcap_path).stat().st_mtime
faketime = datetime.fromtimestamp(mtime).strftime('%Y%m%d%H%M%S')

print('''\
    trust-anchor: ". IN DS 20326 8 2 E06D44B80B8F1D39A95C0B0D7C65D08458E880409BBC683457104237C7F8EC8D" ; TODO: replace, this is hardcoded ICANN TA from 2020
    val-override-date: {time} ; TODO: replace, wild guess from PCAP file modification timestamp
CONFIG_END

SCENARIO_BEGIN TODO: test case generated from PCAP

; forwarder
RANGE_BEGIN 0 1000000
    ADDRESS 192.0.2.1
'''.format(time=faketime))

process_pcap(pcap_path)

print('''
RANGE_END

;; TODO: insert QUERY + CHECK_ANSWER steps

SCENARIO_END''')
