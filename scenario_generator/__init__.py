# coding=utf-8
""" Deckard scenario generator
    ==========================

    Python module for generating test scenarios for testing tool Deckard from live DNS traffic.

    Modules for creating and storing content
    ----------------------------------------

    G{packagetree scenario zone group server}


    Modules for creating output
    ---------------------------

    G{packagetree query steps}

    Module for capturing DNS traffic
    --------------------------------

    Module for capturing DNS traffic between browser and resolver while opening given page

    G{packagetree capture}

    Other modules
    -------------

    G{packagetree common message widgets}

    @author: Filip Široký
    @license: GNU GPL v3.0 or any later version

"""


__all__ = [
    'capture',
    'common',
    'group',
    'message',
    'query',
    'scenario',
    'zone',
    'server',
    'steps',
    'widgets',
]
