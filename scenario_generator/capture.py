#!/usr/bin/env python3
"""
Class for capturing DNS traffic
===============================
"""
import os
import configparser
import subprocess
import time
import concurrent.futures
import multiprocessing
import progressbar
from scenario_generator import common
from scenario_generator import widgets


class Capture:
    """ Capture DNS traffic

    @ivar domains: List of domain names
    @type domains: set

    @ivar tree: Turn on saving PCAPs into separated directories by browsers, resolvers, ...
    @type tree: boolean

    @ivar number: Capture the same domain n times
    @type number: integer

    @ivar verbose: Enable verbose logging
    @type verbose: boolean

    @ivar browsers: List of browsers to be used for capture
    @type browsers: set

    @ivar resolvers: List of resolvers to be used for capture
    @type resolvers: set

    @ivar dir: Directory containing traffic capture scripts
    @type dir: string

    @ivar logs: Directory to save logs in
    @type logs: string

    @ivar output: Directory to save PCAPs in
    @type output: string

    @ivar ident: Capture ID based on the time of creation
    @type ident: string

    @ivar selenium_wait: Time for selenium to wait if browser does not have enough resources to start

                         Mostly for Google Chrome, combination with L{selenium_retry} is advised.
    @type selenium_wait: integer

    @ivar selenium_retry: Number of retries for opening the browser and runing the capture.

                          Mostly for Google Chrome, combination with L{selenium_wait} is advised.
    @type selenium_retry: integer

    @ivar max_workers: Number of threads used to capture the traffic of a list of domains.

                        Defaults to 5 * number of CPUs.
    @type max_workers: integer

    @ivar failed: List of failed captures for final retry
    @type failed: set

    @todo: Switch to logging
    """
    def __init__(self):
        # Source
        self.domains = set()
        # Output
        self.tree = False
        self.number = 1
        self.verbose = False
        # Software
        self.browsers = {"chrome"}
        self.resolvers = {"kresd"}
        # Paths
        self.dir = "./traffic-capture"
        self.logs = "./logs"
        self.output = "./data/pcaps/"
        # Session ID (time)
        self.ident = time.strftime("%Y-%m-%d_%H-%M-%S")
        # Parallelization
        self.selenium_wait = 3
        self.selenium_retry = 3
        self.max_workers = None  # Defaults to number of processors * 5
        # Failed - run again after resources are available
        self.failed = set()

    def apply_args(self, args):
        """ Process parsed command line arguments

        @param args: Parsed command line arguments
        @type args: Namespace
        """
        if args.domains:
            self.domains = set(args.source)
        else:
            for source in args.source:
                file = open(source, "r")
                self.domains |= set(file.read().split())
        # Process configuration
        if args.conf:
            self.process_config(args.conf)
        # Process arguments
        if args.number:
            self.number = args.number
        if args.tree:
            self.tree = True
        if args.output:
            self.output = args.output
        if args.log:
            self.logs = args.log
        if args.verbose:
            self.verbose = True
        if args.browser:
            self.browsers = set(args.browser)
        if args.resolver:
            self.resolvers = set(args.resolver)

    def process_config(self, config):
        """ Process configuration file

        @param config: Configuration file path
        @type config: string
        """
        conf = configparser.ConfigParser()
        conf.read(config)
        if "general" in conf.sections():
            self.tree = conf["general"].getboolean("tree", False)
            self.verbose = conf["general"].getboolean("verbose", False)
            self.number = conf["general"].getint("number", 1)
            self.output = conf["general"].get("output", self.output)
            self.dir = conf["general"].get("dir", self.dir)
            self.logs = conf["general"].get("logs", self.logs)
        if "software" in conf.sections():
            browsers = conf["software"].get("browsers")
            if browsers:
                self.browsers = set(browsers.split())
            resolvers = conf["software"].get("resolvers")
            if resolvers:
                self.resolvers = set(resolvers.split())
        if "parallelization" in conf.sections():
            retries = conf["parallelization"].getint("retries", self.selenium_retry)
            if retries:
                self.selenium_retry = retries
            wait = conf["parallelization"].getint("wait", self.selenium_wait)
            if wait:
                self.selenium_wait = wait
            threads = conf["parallelization"].getint("threads", self.max_workers)
            if threads:
                self.max_workers = threads

    def prepare(self):
        """ Prepare environment (directories) for traffic capture

        @return: 0 if successful, 1 if failed
        @rtype: integer
        """
        if common.create_dir(self.logs) != 0:
            print("Logs directory can't be created and doesn't exist.")
            return 1
        if common.create_dir(self.output) != 0:
            print("Output directory can't be created and doesn't exist.")
            return 1
        if not os.path.exists(self.dir):
            print("Invalid tool directory.")
            return 1
        self.logs = os.path.join(self.logs, self.ident)
        if common.create_dir(self.logs) != 0:
            print("Failed to create log directory")
            return 1
        self.output = os.path.join(self.output, self.ident)
        if common.create_dir(self.output) != 0:
            print("Failed to create output directory")
            return 1
        return 0

    def build_docker(self):
        """ Build Docker images locally

        @return: 0 if successful, 1 if failed
        @rtype: integer
        """
        # Prepare all versions of images
        file = open(os.path.join(self.dir, "Dockerfile-versions"), "r")
        images = set(file.read().split())
        # Verbose output - docker output sent to stdout, not log
        if self.verbose:
            for version in images:
                if subprocess.call(["docker", "build", "--build-arg", "VERSION="+version, "-t",
                                    "deckard/" + version + ":local", self.dir]):
                    print("Building " + version + " image failed")
                    return 1
        else:  # Non-verbose output - progressbar
            pbar = progressbar.ProgressBar(max_value=len(images), redirect_stdout=True,
                                           widgets=widgets.capture_widgets_build())
            pbar.start()
            i = 1
            for version in images:
                log = open(os.path.join(self.logs, "docker-build-" + version), 'w')
                if subprocess.call(["docker", "build", "--build-arg", "VERSION="+version, "-t",
                                    "deckard/" + version + ":local", self.dir], stdout=log, stderr=log):
                    print("Building " + version + " image failed")
                    return 1
                pbar.update(i)
                i += 1
                log.close()
            pbar.finish()
        return 0

    def run_domain(self, data):
        """ Capture the DNS traffic of a desired software
            - browser, resolver, domain and number combination

        @return: Return value of Docker run
        @rtype: integer
        """
        browser = data[0]
        resolver = data[1]
        domain = data[2]
        number = data[3]
        # Prepare for start
        output_dir = "/capture"
        # Real directories
        if self.tree:
            real_output_dir = os.path.abspath(os.path.join(self.output, browser, resolver, domain))
            real_logs_dir = os.path.abspath(os.path.join(self.logs, browser, resolver, domain))
            common.create_dir(real_output_dir)
            common.create_dir(real_logs_dir)
        else:
            real_output_dir = os.path.abspath(self.output)
            real_logs_dir = os.path.abspath(self.logs)
        name = browser + "-" + resolver + "-" + domain + "-" + number
        if browser == "dig":
            image = "chrome"
        else:
            image = browser.split("-")[0]
        # Initiate logging location
        log_name = os.path.join(real_logs_dir, name + ".log")
        symlink = os.path.join(self.logs, "docker-run-last")
        if self.verbose:
            log = subprocess.STDOUT
        else:
            log = open(log_name, "w")
        # Create container and run capture
        ret = subprocess.call(["docker", "run", "--rm",
                               "-e", "BROWSER="+browser,
                               "-e", "RESOLVER="+resolver,
                               "-e", "PAGE="+domain,
                               "-e", "NAME="+name,
                               "-e", "RETRY="+str(self.selenium_retry),
                               "-e", "WAIT="+str(self.selenium_wait),
                               "-v", real_output_dir + ":" + output_dir,
                               "deckard/" + image + ":local"],
                              stdout=log if not self.verbose else None,
                              stderr=log)
        # If error occurred, remove the PCAP, and log
        if ret != 0:
            error_msg = "Creating container from {} failed.\nReturn code: {}\n".format(image, ret)
            if self.verbose:
                print(error_msg)
                progressbar.streams.flush()
            else:
                log.write(error_msg)
            file = os.path.join(real_output_dir, name + ".pcap")
            os.remove(file)
        if not self.verbose:
            # Create symlink to last log file
            if os.path.exists(symlink):
                os.remove(symlink)
            os.symlink(log_name, symlink)
            # Close log file
            log.close()
        return ret

    def run(self, data, workers):
        """ Capture required traffic


        @param data: Browser, Resolver, Domain and Number of capture combinations

               Dictionary with content (browser, resolver, domain, number) = data
        @type data: set

        @param workers: Number of threads required
        @type workers: integer

        @return: 0 if successful, 1 if failed
        @rtype: integer
        """
        count = len(data)
        self.failed = set()
        ret = 0
        # Progressbar
        text, widg = widgets.capture_widgets_run()
        pbar = progressbar.ProgressBar(max_value=count, redirect_stdout=True, widgets=widg)
        pbar.start()
        i = 1
        # Run as many captures as possible in parallel
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=workers)
        future_to_file = dict()
        for item in data:
            future_to_file.update({executor.submit(self.run_domain, item):
                                   item})
        # When one of the captures is finished, update the progressbar
        for future in concurrent.futures.as_completed(future_to_file):
            dataset = future_to_file[future]
            try:
                ret = future.result()
                if ret != 0:
                    self.failed.add(dataset)
            except (concurrent.futures.CancelledError, concurrent.futures.TimeoutError):
                self.failed.add(dataset)

            text.update_mapping(browser=dataset[0],
                                resolver=dataset[1],
                                domain=dataset[2],
                                number=int(dataset[3]),
                                ret="FAIL" if ret else "OK")
            pbar.update(i)
            i += 1
        pbar.finish()

        if self.failed:
            return 1
        return 0

    def run_input(self):
        """ Capture DNS traffic

        Run all combinations of configured resolvers, browsers, domains and multiple times if required.

        @return: 0 if successful, 1 if failed
        @rtype: integer
        """
        data = {(browser, resolver, domain, str(number + 1))
                for browser in self.browsers
                for resolver in self.resolvers
                for domain in self.domains
                for number in range(self.number)}

        return self.run(data, self.max_workers)

    def run_failed(self):
        """ Rerun capture of failed combinations

        @return: 0 if successful, 1 if failed
        @rtype: integer
        """
        return self.run(self.failed, multiprocessing.cpu_count())
