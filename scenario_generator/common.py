#!/usr/bin/env python3
"""
Functions used across the module.
=================================

@group Generic functions: create_dir

@group Functions for working with DNS Message: is_link get_ns_names get_ips get_answer get_rrset

@group Logging functions: get_name log root_log

@group DNS communication functions: send_query resolver resolver_resolve
"""
import os
import errno
import logging
import threading
import dns
import dns.rdatatype


# Generic functions


def create_dir(directory):
    """ If path doesn't exists, create directory.

    Catch any OSError exception.

    @param directory: The directory path to be created.
    @type directory: string

    @return: 0 if successful or already exists, 1 if unsuccessful
    @rtype: integer
    """
    if not os.path.exists(directory):
        try:
            os.makedirs(directory)
            return 0
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                return 1
    else:
        return 0


# DNS functions


def is_link(message):
    """ Determine if the response is a link (contains delegation and should require subdomain match).

    @param message: Response to check
    @type message: dns.message.Message

    @return: True if link, False otherwise or when question name is "."
    @rtype: boolean
    """
    if message.question[0].name.to_text() == ".":
        return False
    if message.question[0].rdtype == dns.rdatatype.NS:
        for rrset in message.answer + message.authority + message.additional:
            if rrset.rdtype == dns.rdatatype.SOA:
                return False
        return True
    return False


def get_ns_names(message):
    """ Get all name servers from the response.

    @param message: Source response
    @type message: dns.message.Message

    @return: Dictionary with NS rdata by name (zone)
    @rtype: Dictionary
    """
    names = {}
    for rrset in message.additional + message.answer + message.authority:
        if rrset.rdtype is dns.rdatatype.NS:
            for rdata in rrset:
                if rrset.name not in names:
                    names[rrset.name] = set()
                names[rrset.name].add(rdata.target)
    return names


def get_ips(message):
    """ Get all IPv4 and IPv6 records from the response.

    @param message: Source response
    @type message: dns.message.Message

    @return: Dictionary with IP's by name (zone)
    @rtype: Dictionary
    """
    ips = {}
    for rrset in message.additional + message.answer + message.authority:
        if rrset.rdtype in [dns.rdatatype.A, dns.rdatatype.AAAA]:
            for rdata in rrset:
                if rrset.name not in ips:
                    ips[rrset.name] = set()
                ips[rrset.name].add(rdata.address)
    return ips


def get_answer(message):
    """ Get list of answer's rdata (CNAME, DNAME, A, AAAA, SOA).

    @param message: Source response
    @type message: dns.message.Message

    @return: List of rdata
    @rtype: set
    """
    question = message.question[0].name
    answer = set()
    for rrset in message.answer + message.additional + message.authority:
        if rrset.name == question:
            for rdata in rrset:
                # Check if answered: maybe additional rdatatypes
                if rrset.rdtype in (dns.rdatatype.CNAME, dns.rdatatype.DNAME):
                    answer.add(rdata.target)
                elif rrset.rdtype in (dns.rdatatype.A, dns.rdatatype.AAAA):
                    answer.add(rdata.address)
                elif rrset.rdtype == dns.rdatatype.SOA:
                    answer.add(rdata.mname)
    return answer


def get_rrset(message, name=None, rdatatype=dns.rdatatype.CNAME):
    """ Get requested rrset from DNS message.

    @param message: Source response
    @type message: dns.message.Message

    @param name: Name of the required rrset
    @type name: dns.name.Name

    @param rdatatype: Rdatatype of the rrset
    @type rdatatype: integer

    @return: Requested rrset if exists
    @rtype: dns.rrset.RRset or None
    """
    if not name:
        name = message.question[0].name
    return message.get_rrset(message.answer, name=name, rdclass=dns.rdataclass.IN, rdtype=rdatatype)

# Logging wrappers


def get_name():
    """ Get current threads name

    @return: Thread name (source file name)
    @rtype: string
    """
    return threading.currentThread().getName()


def log():
    """ Get logger for current thread.

    @return: thread specific logger
    @rtype: logging.Logger
    """
    return logging.getLogger(get_name())


def root_log():
    """ Get logger for root to log onto stdout.

    @return: root logger
    @rtype: logging.Logger
    """
    return logging.getLogger("root")

# DNS communication


class Cache:
    """ Name server responses cache.

    Cache is addressed by a tuple: (ip, question name, question rdtype, question rdclass)

    @ivar cache: Cache
    @type cache: dictionary

    @todo: Records with ttl to avoid SIGKILL
    """
    def __init__(self):
        self.cache = {}

    def get(self, key):
        """ Get cache content for given key.

        @param key: Tuple identifying the response and the source name server.
        @type key: tuple

        @return: Cached response
        @rtype: dns.message.Message
        """
        if key in self.cache:
            return self.cache[key]
        return None

    def put(self, key, value):
        """ Put response for given key into the cache.

        @param key: Tuple identifying the response and the source name server.
        @type key: tuple

        @param value: DNS response from the server.
        @type value: dns.message.Message
        """
        self.cache[key] = value


CACHE = Cache()
""" Cache shared by all threads """


def send_query(message, addr, timeout=3, retry=3, want_dnssec=True):
    """ Query the name server.

    Adjusted dns.resolver.Resolver.query code.

    @param message: Message to send
    @type message: dns.message.Message

    @param addr: The IP address of the given server
    @type addr: string

    @param timeout: Accepted timeout for the server
    @type timeout: integer

    @param retry: Number of retries if the query failed
    @type retry: integer

    @param want_dnssec: Want DNSSEC records in the response
    @type want_dnssec: boolean

    @return: Response to the query
    @rtype: dns.message.Message or None if fails
    """
    global CACHE
    if CACHE:
        answer = CACHE.get((addr, message.question[0].name, message.question[0].rdtype, message.question[0].rdclass))
        if answer is not None and answer.answer is not None:
            return answer
    # Force EDNS 4096 datagram size and DNSSEC
    message.use_edns(True, 1, 4096, 4096)
    if want_dnssec:
        message.want_dnssec()
    do_retry = retry
    response = None
    while do_retry:
        try:
            response = dns.query.udp(message, addr, timeout, ignore_trailing=True)
            if response.flags & dns.flags.TC:
                # Response truncated; retry with TCP.
                response = dns.query.tcp(message, addr, timeout, ignore_trailing=True)
        except dns.exception.DNSException:
            log().exception("DNS query %s to %s caused exception", message.to_text(), addr)

        # Retry if not answered
        if response:
            rcode = response.rcode()
            if rcode == dns.rcode.NOERROR or rcode == dns.rcode.NXDOMAIN:
                break
            # When
            if rcode == dns.rcode.FORMERR:
                message.use_edns(False)
                do_retry += 1  # If EDNS is not supperted, try without it
        do_retry = do_retry - 1

    if CACHE:
        CACHE.put((addr, message.question[0].name, message.question[0].rdtype, message.question[0].rdclass), response)
    return response


def resolver(name, rdtype):
    """ Query default resolvers.

    @param name: Domain name to query
    @type name: dns.name.Name or string

    @param rdtype: Rdata type to query
    @type rdtype: integer

    @return: Response to the query
    @rtype: dns.message.Message
    """
    message = dns.message.make_query(name, rdtype)
    response = None
    resolv = dns.resolver.get_default_resolver()
    for addr in resolv.nameservers:
        response = send_query(message, addr, want_dnssec=False)
        if response:
            break
    return response


def resolver_resolve(name, rdtype):
    """ Get rdata to a query from resolver.

    @param name: Domain name to query
    @type name: dns.name.Name or string

    @param rdtype: Rdata type to query
    @type rdtype: integer

    @return: Rdata from the response
    @rtype: set of string and dns.name.Name's
    """
    answer = set()
    response = resolver(name, rdtype)
    if response:
        for rrset in response.answer:
            for rdata in rrset:
                if rrset.rdtype == dns.rdatatype.NS:
                    answer.add(rdata.target)
                else:
                    answer.add(rdata.to_text())
    return answer
