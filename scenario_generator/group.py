#!/usr/bin/env python3
"""
Group of name servers
=====================
"""
import subprocess
import dns
from scenario_generator.server import Server
from scenario_generator import common
from scenario_generator.message import HashableMessage


class Group:
    """ This class groups name servers expected to have the same content.
        Represents scenarios RANGE.

        @ivar names: set of names belonging to the group name servers
        @type names: set of dns.name.Name

        @ivar ips: set of IP's belonging to the group name servers
        @type ips: set of strings

        @ivar names_ips: name server names mapped to their IP's

              Dictionary keys are dns.name.Name objects

              Dictionary values are sets of strings
        @type names_ips: dictionary

        @ivar content: All DNS messages the group should respond to.

              Dictionary keys are string representation of DNS message question

              Dictionary values are dns.message.Message objects
        @type content: dictionary

        @ivar servers: L{Server} objects of name servers in the group.

              Dictionary keys are IP's of the name servers

              Dictionary values are L{Server} objects for the key IP
        @type servers: dictionary

        @ivar working: set of working name servers from the group
        @type working: set of IP's

        @ivar not_working: set of name servers from the group that don't seem to be working
        @type not_working: set of IP's

        @ivar no_ipv6: Scenario setting - don't use ipv6
        @type no_ipv6: boolean

        @ivar one_server: Scenario setting - get response only from one server
        @type one_server: boolean

        @ivar final_server: L{Server} object with answers to all queries required from the group after L{finalize}
        @type final_server: L{Server} object

        @ivar zones: List of zones the group of name servers is authoritative for
        @type zones: set

        @todo: Add zone information to L{zones} instantly (and remove it when needed)
        @todo: If response is not AA (or doesnt contain answer) try other servers first
        @todo: Add first response to L{final_server} right away
    """

    def __init__(self, names, ips=None, ips_names=None, no_ipv6=True, one_server=True, zones=None):
        self.names = names
        self.ips = ips.copy() if ips else set()
        self.names_ips = ips_names.copy() if ips_names else dict()
        self.content = dict()
        self.servers = dict()
        # Servers
        if ips:
            self.working = set(addr for addr in ips.copy() if addr.find(":") == -1 or not no_ipv6)
        else:
            self.working = set()
        self.not_working = set()
        # Settings
        self.no_ipv6 = no_ipv6
        self.one_server = one_server
        # Output
        self.zones = zones if zones else set()
        self.final_server = None

    def __str__(self):
        """ Get printable list of group content.

        @return: L{Group}
        @rtype: string
        """
        string = "Group:\n\tIPs and Names:\n"
        tmp_string = ""
        for name, ips in self.names_ips.items():
            tmp_string += "\t\t{0} = {1}\n".format(name, ", ".join(sorted(ips)))
        string += "".join(sorted(tmp_string.splitlines(True)))
        unused_names = self.names - self.names_ips.keys()
        unused_ips = self.ips - set.union(*self.names_ips.values()) if self.names_ips else None
        if unused_names:
            string += "\tOther names:\n"
            for dname in sorted(unused_names):
                string += "\t\t{0}\n".format(dname)
        if unused_ips:
            string += "\tOther ips:\n"
            for addr in sorted(unused_ips):
                string += "\t\t{0}\n".format(addr)
        # Add list zones
        string += "\tZones:\n"
        for zone in self.zones:
            string += "\t\t{0}\n".format(zone)
        # Add list of queries in content
        string += "\tContent:\n"
        for dname in sorted(self.content):
            string += "\t\t{0}\n".format(dname)
        # Add list of server ips
        string += "\tServers:\n"
        for server in self.servers:
            string += "\t\t{0}\n".format(server)
        return string

    def to_string(self):
        """ Get the RANGE string out of the zone.

        @return: RANGE as string
        @rtype: string
        """
        if not self.ips:
            return ""
        string = "; Group's zones:\n"
        for zone in self.zones:
            string += ";\t{0}\n".format(zone)
        string += "; Server names:\n"
        for name in self.names:
            string += ";\t{0}\n".format(name)
        string += "RANGE_BEGIN 0 1000\n"
        for addr in self.ips:
            string += "\tADDRESS {0}\n".format(addr)
        string += "\n"
        string += self.final_server.to_string_content()
        string += "\n\nRANGE_END\n\n"
        return string

    def update(self, names=None, names_ips=None, resolve_names=False):
        """ Update the Group based on its names and other input.

        Remove names_ips keys that are not in L{names}.

        Update ips based on L{names}.

        Add new IP's to working.

        @param names: set new list of name server names
        @type names: set of dns.name.Name

        @param names_ips: Names mapped to IP's used to update groups current list.
        @type names_ips: dictionary

        @param resolve_names: Resolve IP addresses of name servers.
        @type resolve_names: boolean
        """
        # Update names if required
        if names:
            self.names = names
        # Update names_ips from new
        if names_ips:
            for name, ips in names_ips.items():
                if name in self.names:
                    if name in self.names_ips:
                        self.names_ips[name] |= names_ips[name]
                    else:
                        self.names_ips[name] = names_ips[name]
                    self.ips |= names_ips[name]
        # Resolve names if required
        if resolve_names:
            self.resolve_names()
        # Spread the change in content
        names_to_remove = set()
        for name in self.names_ips.keys():
            if name not in self.names:
                names_to_remove.add(name)
        # Remove names_ips that do not belong in the group
        for name in names_to_remove:
            self.names_ips.pop(name, None)
        # Update IPs
        new_ips = set()
        for name, ips in self.names_ips.items():
            new_ips |= ips
        self.ips = new_ips
        # Update working
        self.working = set(addr for addr in self.ips.copy() if addr.find(":") == -1 or
                           not self.no_ipv6) - self.not_working
        self.not_working &= self.ips

    def get_name(self, ip_address):
        """ Get name of the name server with the IP

        @param ip_address: IP of the name server
        @type ip_address: string

        @return: name of the name server
        @rtype: dns.name.Name
        """
        for name, ips in self.names_ips.items():
            if ip_address in ips:
                return name
        return None

    def resolve_names(self):
        """ Get IPv4 and IPv6 for each name server name and update L{names_ips}, L{working} and L{ips} """
        for name in self.names:
            if name not in self.names_ips or not self.names_ips[name]:
                ipv4_rdata = common.resolver_resolve(name, dns.rdatatype.A)
                ipv6_rdata = common.resolver_resolve(name, dns.rdatatype.AAAA)
                # Add IPs to the qroup
                ips = set()
                for ip_rdata in ipv4_rdata | ipv6_rdata:
                    self.ips.add(ip_rdata)
                    ips.add(ip_rdata)
                # Add new IPs to working
                new4 = ipv4_rdata - self.working - self.not_working
                new6 = ipv4_rdata - self.working - self.not_working
                self.working |= new4
                if not self.no_ipv6:
                    self.working |= new6
                self.names_ips[name] = ips

    def send_query(self, message, aa_only=False):
        """ Send DNS query to the name servers and add its question to content.

        If no response was given, add SERVFAIL.

        If server fails to respond, ping it.
        If ping fails, remove it from L{working}.

        @param message: DNS message with to be sent
        @type message: dns.message.Message

        @param aa_only: Accept the response and add the question to the content only if answer had AA flag set to 1.
        @type aa_only: boolean

        @return: Received responses
        @rtype: set of dns.message.Message

        @raise Exception: No response - if server fails to response while responses from all name servers are required

        @todo: Detect server unresponsive to DNS queries
        """
        query = message.question[0].to_text()
        possible_response = None
        responses = set()
        not_working = set()
        # Iterate over servers
        for addr in self.working:
            response = common.send_query(message, addr)
            # response = self.resolve_server(query, ip, scope, any_response)
            if not response:
                # print(ip, type(ip))
                if ":" not in addr:
                    ping = subprocess.run("ping -c 1 " + addr, shell=True, stdout=subprocess.DEVNULL)
                else:
                    ping = subprocess.run("ping6 -c 1 " + addr, shell=True, stdout=subprocess.DEVNULL)
                if ping.returncode != 0:
                    not_working.add(addr)
                # If responses from all servers are required raise exception (Maybe add timeout instead later)
                if not self.one_server:
                    raise Exception("No response from " + addr)
            elif self.one_server and response.rcode() != dns.rcode.NOERROR:
                # Skip ERROR if looking only for one response, try to get proper response
                possible_response = [addr, response]
            elif not aa_only or response.flags & dns.flags.AA:
                hmessage = HashableMessage(response)
                responses.add(hmessage)
                if addr not in self.servers:
                    self.servers[addr] = Server(addr, self.get_name(addr))
                self.servers[addr].add_query(response)
                if self.one_server:
                    break
        # Update working and not working
        for addr in not_working:
            self.working.remove(addr)
            self.not_working.add(addr)
        # If no better answer received, return ERROR
        if possible_response:
            addr = possible_response[0]
            response = possible_response[1]
            if not aa_only or response.flags & dns.flags.AA:
                hmessage = HashableMessage(response)
                responses.add(hmessage)
                self.servers[addr] = Server(addr, self.get_name(addr))
                self.servers[addr].add_query(response)
        # Add the query to the content if response accepted
        if responses or not aa_only:
            if query not in self.content:
                self.content[query] = message
        # If group fails to respond - add servfail
        if not responses and not aa_only:
            response = dns.message.make_response(message)
            response.set_rcode(dns.rcode.SERVFAIL)
            for addr, server in self.servers.items():
                server.add_query(response)
                if self.one_server:
                    return responses
        # Return received answers
        return responses

    def finalize(self):
        """ Finalize the groups content for finishing the scenario.

        Create L{final_server} and fill it with present responses. If response is missing, get it.

        @bug: final_server not working sometimes (ipv6 when ipv6 is not available, ...)
        """
        if not self.servers.values():
            common.log().debug("No servers present %s", str(self.names))
            return
        for server in self.servers.values():
            if not self.final_server:
                self.final_server = server
            if self.content.keys() - self.final_server.present:
                if server.present - self.final_server.present:
                    # Update links
                    for key in server.links.keys():
                        if key not in self.final_server.links:
                            self.final_server.links[key] = server.links[key]
                        else:
                            self.final_server.links[key].update(server.links[key])
                    # Update answered
                    self.final_server.answered.update(server.answered)
                    # Update present
                    self.final_server.present.update(server.present)
            else:
                break
        # Query content not present in the server
        for question, message in self.content.items():
            if question not in self.final_server.present:
                response = common.send_query(message, self.final_server.ip_address)
                if not response:
                    response = dns.message.make_response(message)
                    response.set_rcode(dns.rcode.SERVFAIL)
                self.final_server.add_query(response)


def get_groups(message, one_server):
    """ Get groups from the response.

    @param message: Source response
    @type message: dns.message.Message

    @param one_server: One server setting from scenario
    @type one_server: boolean

    @return: Dictionary with groups by name (zone)
    @rtype: Dictionary
    """
    ips = common.get_ips(message)
    names = common.get_ns_names(message)
    # Groups IP's by NS names
    groups = {}
    for rrset_name, name_list in names.items():
        ip_set = set()
        names_ips = dict()
        for name in name_list:
            if name in ips:
                ip_set |= ips[name]
                names_ips[name] = ips[name]
        group = Group(names=name_list, ips=ip_set, ips_names=names_ips, one_server=one_server)
        groups[rrset_name] = group
    return groups
