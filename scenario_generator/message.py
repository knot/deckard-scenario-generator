#!/usr/bin/env python3
"""
Hashable DNS message
====================
"""
import dns.message


class HashableMessage(dns.message.Message):
    """ Hashable dns.message.Message
    """
    def __init__(self, parent):
        """ Update the class variables from dns.message.Message object """
        super().__init__()
        self.__dict__.update(parent.__dict__)

    def __hash__(self):
        """ Hash dns.message.Message by hashing its string format

        @return: Hashed dns.message.Message
        @rtype: integer
        """
        return hash(self.to_text())
