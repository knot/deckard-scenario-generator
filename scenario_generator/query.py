#!/usr/bin/env python3
"""
Query representing ENTRY
========================
"""
import dns
import dns.resolver


class Query:
    """ Class for converting DNS messages to Deckard's entries.

    @ivar question: DNS messages question section
    @type question: string

    @ivar answer: DNS messages answer section
    @type answer: string

    @ivar additional: DNS messages additional section
    @type additional: string

    @ivar authority: DNS messages authority section
    @type authority: string

    @ivar flags: DNS messages flags
    @type flags: string

    @ivar ident: DNS messages id
    @type ident: integer

    @ivar step: Deckard's step ID (used only if the query is a step)
    @type step: integer

    @ivar link: Is query a delegation? (MATCH subdomain is used)
    @type link: boolean

    @ivar other_answer: Alternative RRset for STEP CHECK_ANSWER (commented)
    @type other_answer: string

    @todo: Sort individual RRsets (make CNAMEs first in correct order)
    """
    def __init__(self, query=None, other=None, step=0, link=False):
        self.question = ""
        self.answer = ""
        self.additional = ""
        self.authority = ""
        self.flags = ""
        self.ident = 0
        self.step = step
        self.link = link
        # Add alternative RRSET for CNAME
        self.other_answer = ""
        if query:
            self.query_from_packet(query)
        if other:
            if other != query.get_rrset(query.answer, other.name, dns.rdataclass.IN, other.rdtype):
                self.other_answer += '; ' + '; '.join(other.to_text().splitlines(True)) + '\n'

    def to_string(self, server=False):
        """ Get string representation of the entry.

        @param server: Entry is in RANGE, False means entry is STEP
        @type server: boolean

        @return: Entry with proper fields
        @rtype: string
        """
        question = ''
        answer = ''
        auth = ''
        additional = ''
        self.sort_records()
        if self.question != '':
            question = 'SECTION QUESTION\n' + self.question
        if self.authority != '':
            auth = "SECTION AUTHORITY\n" + self.authority
        if self.additional != '':
            additional = 'SECTION ADDITIONAL\n' + self.additional
        if self.answer != '':
            answer = 'SECTION ANSWER\n' + self.answer
            if self.other_answer:
                answer += self.other_answer
        if server:
            query_string = 'ENTRY_BEGIN\n'
            query_string += 'MATCH {0}\n'.format('subdomain' if self.link else 'qname  qtype')
            query_string += 'ADJUST copy_id{0}\n'.format(' copy_query' if self.link else '')
        elif 'QR' in self.flags.split():
            query_string = 'STEP {0} CHECK_ANSWER\n'.format(self.step)
            query_string += 'ENTRY_BEGIN\n'
            query_string += 'MATCH opcode flags rcode question answer\n'
        else:
            query_string = 'STEP {0} QUERY\n'.format(self.step)
            query_string += 'ENTRY_BEGIN\n'
        query_string += 'REPLY {0}\n'.format(self.flags)
        query_string += '{0}{1}{2}{3}'.format(question, answer, auth, additional)
        query_string += 'ENTRY_END\n'
        return query_string

    def query_from_packet(self, dnsmsg):
        """ Fill entry fields from DNS message.

        @param dnsmsg: Source DNS message
        @type dnsmsg: dns.message.Message
        """
        # Query
        self.set_flags(dnsmsg.flags)
        self.ident = dnsmsg.id
        # Question section
        if dnsmsg.question:
            for question in dnsmsg.question:
                self.question += question.to_text() + '\n'
        # Additional section
        if dnsmsg.additional:
            for additional in dnsmsg.additional:
                self.additional += additional.to_text() + '\n'
        # Answer section
        if dnsmsg.answer:
            for answer in dnsmsg.answer:
                self.answer += answer.to_text() + '\n'
        # Authority section
        if dnsmsg.authority:
            for authority in dnsmsg.authority:
                self.authority += authority.to_text() + '\n'

    def set_flags(self, flags):
        """ Set flags in string format from integer.

        @param flags: Flags value to be set
        @type flags: integer
        """
        self.flags = dns.flags.to_text(flags)
        if 'QR' in self.flags.split():
            self.flags += " " + dns.rcode.to_text(flags & 0b1111)

    def sort_records(self):
        """ Sort entry's sections """
        self.answer = sort_section(self.answer)
        self.question = sort_section(self.question)
        self.additional = sort_section(self.additional)
        self.authority = sort_section(self.authority)


def sort_section(section):
    """ Sort DNS section RRset lines.

    @param section: Section RRset to be sorted
    @type section: string

    @return: Sorted RRset
    @rtype: string

    @todo: Sort individual RRsets (make CNAMEs first in correct order)
    """
    if section == '':
        return section
    lines = filter(None, section.split('\n'))
    return '\n'.join(sorted(lines)) + '\n'
