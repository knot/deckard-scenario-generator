#!/usr/bin/env python3
"""
Deckard scenario
================
"""
import os
import copy
import datetime
import socket
import logging
import threading
import progressbar
import dpkt
import dns.exception
import dns.resolver
from scenario_generator.group import Group, get_groups
from scenario_generator.server import Server
from scenario_generator.steps import Steps
from scenario_generator.zone import Zone
from scenario_generator import common


class Scenario:
    """ Deckard scenario class

    @ivar root_ip: One of the root name server IPs
    @type root_ip: string

    @ivar steps: Deckard's steps
    @type steps: L{Steps}

    @ivar name: Name of the scenario
    @type name: string

    @ivar no_ipv6: Setting for not using IPv6
    @type no_ipv6: boolean

    @ivar one_server: Setting for using requiring only one server
    @type one_server: boolean

    @ivar servers: L{Server} objects addressed by their IP
    @type servers: dictionary

    @ivar groups: List of current L{Group} objects
    @type groups: list

    @ivar zones: L{Zone} objects addressed by their origin
    @type zones: dictionary

    @ivar resolved: List of resolved DNS queries

                    Domain name as key, rdtypes as items in set
    @type resolved: dictionary
    """
    def __init__(self, no_ipv6=False, one_server=False):
        self.root_ip = None
        self.steps = Steps()
        self.name = "Unnamed"
        # Scenario parameters
        self.no_ipv6 = no_ipv6
        self.one_server = one_server
        self.servers = dict()  # IP:Server name: servers (ips - ipv4/ipv6, names(if multiple), ipv4 and ipv6 content)
        self.groups = list()   # List of all groups
        self.zones = dict()    # name (by labels separated content) - root, .com, .org, ...
        self.resolved = dict()
        # Prepare root zone
        self.prepare_root_ip()

    def to_string(self):
        """ Get the Scenario string representation.

        @return: Scenario as string
        @rtype: string
        """
        date = datetime.datetime.today()
        scenario_string = '\ttrust-anchor: ". IN DS 19036 8 2 ' \
                          '49AAC11D7B6F6446702E54A1607371607A1A41855200FD2CE1CDDE32F24E8FB5"\n'\
                          '\ttrust-anchor: ". IN DS 20326 8 2 ' \
                          'E06D44B80B8F1D39A95C0B0D7C65D08458E880409BBC683457104237C7F8EC8D"\n'\
                          '\tval-override-date: {0}{1}{2}{3}{4}{5}\n'.format(date.year,
                                                                             str(date.month).zfill(2),
                                                                             str(date.day).zfill(2),
                                                                             str(date.hour).zfill(2),
                                                                             str(date.minute).zfill(2),
                                                                             str(date.second).zfill(2))

        scenario_string += "\tstub-addr: {0}\n".format(self.root_ip)
        scenario_string += "CONFIG_END\n\nSCENARIO_BEGIN {0}\n\n".format(self.name)
        for group in self.groups:
            scenario_string += group.to_string() + '\n\n'
        scenario_string += "\n\n; Sequence of queries made by browser\n\n"
        scenario_string += self.steps.to_string()
        scenario_string += "SCENARIO_END"
        return scenario_string

    def update(self, answers):
        """ Update Zones and Groups based on received responses

        @param answers: Set of DNS messages
        @type answers: set of dns.message.Message

        @return: RDATA from authoritative answer
        @rtype: set

        @attention: Major function. Handles content grouping.
        """
        rdata = set()    # IP's, DNAME and CNAME domain names (from AA)
        groups = dict()  # Groups retrieved from responses
        # Get RDATA (IP, DNAME), groups from responses
        for answer in answers:
            rdata.update(common.get_answer(answer))
            groups.update(get_groups(answer, self.one_server))
        # Get new groups and add them to the zone
        groups = self.get_groups_unique(groups)
        self.add_groups_to_zone(groups)

        # Update servers based on groups
        current_servers = dict()
        names_ips = dict()
        # Ensure all IPs are known
        for group in self.groups:
            group.resolve_names()
            names_ips.update(group.names_ips)
            current_servers.update(group.servers)
        # Get servers created in group (just in case)
        for addr in current_servers:
            if addr not in self.servers.keys():
                self.servers[addr] = current_servers[addr]
        # Create missing servers
        for name, ips in names_ips.items():
            for addr in ips:
                if addr not in self.servers:
                    self.servers[addr] = Server(ip_addr=addr, name=name)
        # Add servers to the group they belong to
        for group in self.groups:
            group.servers = dict((k, self.servers[k]) for k in group.ips if k in self.servers.keys())
        # Return found RDATA
        return rdata

    def resolve(self, name, rdtype=dns.rdatatype.A):
        """ Resolve name using provided query

        Resolve query label by label getting NS, required rdtype and other required records.

        @param name: Domain name to resolve
        @type name: dns.name.Name

        @param rdtype: Required rdtype
        @type rdtype: integer

        @return: RDATA from authoritative answer
        @rtype: set
        """
        # Avoid resolving resolved
        if name in self.resolved and rdtype in self.resolved[name]:
            return set()
        else:
            if name not in self.resolved:
                self.resolved[name] = set()
            self.resolved[name].add(rdtype)
        # Variables
        zone = None
        rdata = set()
        rdtypes = {dns.rdatatype.A, dns.rdatatype.AAAA, rdtype}  # , dns.rdatatype.NS}?
        # Resolve domain name - for each zone gets DS, DNSKEY, NS and RDTYPE for each label
        for i in range(1, len(name) + 1):
            # Prepare content
            if name.split(i)[1] in self.zones:
                zone = name.split(i)[1]
            elif not zone:
                raise RuntimeError("Root zone missing.")
            if i < len(name):
                query = name.split(i+1)[1]
            else:
                query = name
            answers = set()
            # Try full name: www.nrl.navy.mil - navy.mil and nrl.navy.mil are in one zone.
            # This has significant performance impact (up to double the time to create the same scenarios)
            # Optimization by tracking AA delegations might reduce the time significantly.
            for rdatatype in rdtypes:
                answers.update(self.zones[zone].send_query(name, rdatatype, aa_only=True))
            # Resolve QMIN links: NS, DS, DNSKEY
            answers.update(self.zones[zone].send_query(query, dns.rdatatype.NS))
            rdata.update(self.update(answers))
        # Resolve query - now at required zone
            if name == query:
                answers = set()
                if name in self.zones:  # Maybe obsolete?
                    zone = name
                for rdatatype in rdtypes:
                    answers.update(self.zones[zone].send_query(name, rdatatype))
                rdata.update(self.update(answers))
        # Return IP's, DNAME and CNAME domain names
        return rdata

    def resolve_all(self, name, rdtype=dns.rdatatype.A):
        """ Resolve required domain name and all domain names located in the response

        @param name: Domain name to resolve
        @type name: dns.name.Name

        @param rdtype: Required rdtype
        @type rdtype: integer

        @return: RDATA from authoritative answer (IPv4 and IPv6 only)
        @rtype: set
        """
        ips = set()  # Put All resolved IP's here
        answers = self.resolve(name, rdtype)
        # Resolve newly found domain names
        for answer in answers:
            if isinstance(answer, dns.name.Name):
                ips |= self.resolve_all(answer)
            else:
                ips.add(answer)
        return ips

    def split_groups(self, original, found):
        """ Split groups and clear server content to avoid conflicts

        Remove all names common for both groups from found group.
        If original group contains only the common names, then return it.
        If there are names in original group, that are not in common, create new group for common names and return
        both original and new common group.

        @param original: Group that is used in the scenario
        @type original: L{Group}

        @param found: New group from rrset
        @type found: L{Group}

        @return: Relevant groups (Original group, [common servers group])
        @rtype: set
        """
        # Get both names-ips
        all_names_ips = copy.deepcopy(original.names_ips)
        for key in found.names_ips.keys():
            if key not in all_names_ips:
                all_names_ips[key] = found.names_ips[key]
            else:
                all_names_ips[key].update(found.names_ips[key])
        # Get common names
        common_names = original.names & found.names
        # Remove now used name servers from found group
        found.names -= common_names
        found.update(names_ips=all_names_ips, resolve_names=True)
        # Apply split
        if original.names - common_names:
            # Reduce original group
            original.names -= common_names
            original.update(names_ips=all_names_ips, resolve_names=True)
            # Create new group based on common name servers
            common_servers = Group(common_names, one_server=self.one_server, no_ipv6=self.no_ipv6)
            common_servers.update(names_ips=all_names_ips, resolve_names=True)
            self.groups.append(common_servers)
            for server in list(original.servers.values()) + list(common_servers.servers.values()):
                server.links = dict()
                server.answered = set()
                server.present = set()
            return {common_servers, original}
        return {original}

    def get_groups_unique_group(self, group):
        """ Split group into multiple groups based on already existing groups

        @param group: Group to split
        @type group: L{Group}

        @return: Relevant groups the group was split into
        @rtype: set
        """
        relevant_groups = set()
        for original in self.groups:
            if original.names == group.names:
                original.update(names_ips=group.names_ips)
                relevant_groups.add(original)
                return relevant_groups
            elif original.names & group.names:
                relevant_groups.update(self.split_groups(original, group))

        if group.names:
            self.groups.append(group)
            relevant_groups.add(group)
        return relevant_groups

    def get_groups_unique(self, groups):
        """ Take groups received from answers and get apply them to already existing groups

        @param groups: New groups by zone
        @type groups: dictionary

        @return: Groups to be added to the zone. Both original and new.
        @rtype: dictionary
        """
        add_groups = {}
        for name, group in groups.items():
            if name not in add_groups:
                add_groups[name] = set()
            groups_to_add = self.get_groups_unique_group(group)
            if groups_to_add:
                add_groups[name] |= groups_to_add
        return add_groups

    def add_groups_to_zone(self, groups_by_zone):
        """ Add group to the zone

        @param groups_by_zone: Groups to be added to the zone based on key
        @type groups_by_zone: dictionary
        """
        for name, groups in groups_by_zone.items():
            if name not in self.zones:
                self.zones[name] = Zone(name=name, groups=groups, one_server=self.one_server)
            else:
                for group in groups:
                    if group not in self.zones[name].groups:
                        self.zones[name].groups.add(group)

    def process_dns(self, dnsmsg, src_ip, dst_ip):
        """ Process DNS packet from PCAP

        If the packet comes from or to the resolver (must be 127.0.0.1) add it to steps.
        If the message is request, resolve it.

        @param dnsmsg: DNS message
        @type dnsmsg: dns.message.Message

        @param src_ip: Source IP address
        @type src_ip: string

        @param dst_ip: Destination IP address
        @type dst_ip: string
        """
        # Browser - Resolver
        if src_ip == '127.0.0.1' and dst_ip == '127.0.0.1':
            self.steps.add_query(dnsmsg)
            if dnsmsg.flags & dns.flags.QR == 0:
                self.resolve_all(dnsmsg.question[0].name, dnsmsg.question[0].rdtype)

    def log(self):
        """ Log the content of L{Zone}, L{Group} and L{Server} objects"""
        self.log_zones()
        self.log_groups()
        self.log_servers()

    def log_zones(self):
        """ Log scenario zones """
        common.log().debug("Scenario %s zones", self.name)
        for zone in self.zones.values():
            common.log().debug(zone)

    def log_groups(self):
        """ Log scenario groups """
        common.log().debug("Scenario %s groups", self.name)
        for group in self.groups:
            common.log().debug(group)

    def log_servers(self):
        """ Log scenario servers """
        common.log().debug("Scenario %s servers", self.name)
        for server in self.servers.values():
            # if len(server.answered) != 0 and len(server.links) != 0:
            common.log().debug(str(server))

    def prepare_root_ip(self):
        """ Resolve root name servers and their IPs. Create root zone.

        @raise RuntimeError: If resolving root servers fails
        """
        root = dns.name.from_text(".")
        # Create root name servers group
        try:
            name_servers = common.resolver_resolve(".", dns.rdatatype.NS)
            root_group = Group(names=name_servers, no_ipv6=self.no_ipv6, one_server=self.one_server)
            root_group.resolve_names()
            self.zones[root] = Zone(root, groups={root_group}, one_server=self.one_server)
            self.groups.append(root_group)
            self.root_ip = list(root_group.ips)[0]
        except Exception:
            common.log().exception("Root name server preparation failed")
            raise RuntimeError

    def resolve_ns(self):
        """ Resolve domain names of all name servers from all zones.

        If the list of names changes (new groups of servers), repeat.
        """
        before_names = set()
        after_names = set()
        # Get current set of all name server names
        for group in self.groups:
            before_names.update(group.names_ips.keys())
        # Resolve all current name server names
        for group in self.groups:
            for name in copy.deepcopy(group.names):
                self.resolve_all(name)
        # Get new set of all name server names
        for group in self.groups:
            after_names.update(group.names_ips.keys())
        if after_names - before_names:
            self.resolve_ns()

    def process_file(self, file):
        """ Process file by resolving all queries in the PCAP

        @param file: Path to the PCAP file
        @type file: basestring
        """
        # Process file
        file_pcap = open(file, 'rb')
        pcap = dpkt.pcap.Reader(file_pcap)
        # Process each packet
        for packet in pcap:
            # Process layers
            frame = dpkt.sll.SLL(packet[1])
            address = frame.data
            transport = address.data
            # Get IP's from IP layer
            src_ip = socket.inet_ntoa(address.src)
            dst_ip = socket.inet_ntoa(address.dst)
            # Process DNS layer
            # Continue only if next layer (dns) present
            if transport.data != b'':
                # Remove DNS over TCP specific length field
                if isinstance(transport, dpkt.tcp.TCP):
                    transport.data = transport.data[2:]
                # Parse wire to message
                # noinspection PyBroadException
                try:
                    dnsmsg = dns.message.from_wire(transport.data)
                except dns.exception.DNSException:  # Skip unsupported dns packet
                    continue
                self.process_dns(dnsmsg, src_ip, dst_ip)

        self.finalize()

    def __get_child_ds(self, length, zones, zone):
        """ Get DS records for all known child zones

        @param length: Current zones length
        @type length: integer

        @param zones: List of known zones addressed by their length
        @type zones: dictionary

        @param zone: Current zone
        @type zone: dns.name.Name

        @todo: Return or resolve new domain names
        """
        for follows in range(length + 1, max(zones.keys()) + 1):
            if follows not in zones:
                continue
            answers = set()
            for other in zones[follows]:
                if other.is_subdomain(zone) and self.zones[zone].signed:
                    answers.update(self.zones[zone].send_query(other, dns.rdatatype.DS))
                    for child in self.zones[zone].signed_child:
                        self.zones[child].signed = True
            self.update(answers)

    def per_zone_content(self):
        """ Add content required from all zones

        Require DNSSEC only if DS exists in parent zone.
        Add NS, DS, DNSKEY and possibly other records if needed.
        Update the scenario based on received answers.

        @todo: Return or resolve new domain names
        """
        # Prepare zones for chain of trust control
        zones = dict()
        for zone in self.zones:
            if len(zone) not in zones:
                zones[len(zone)] = set()
            zones[len(zone)].add(zone)
            if zone not in zones[len(zone)]:
                zones[len(zone)][zone] = False
        self.zones[dns.name.from_text(".")].signed = True
        # Add zone specific content from root to all other zones
        for length in sorted(zones.keys()):
            for zone in zones[length]:
                answers = set()
                answers.update(self.zones[zone].send_query(zone, dns.rdatatype.NS))
                # Add own DNSSEC records
                if self.zones[zone].signed:
                    answers.update(self.zones[zone].send_query(zone, dns.rdatatype.DS))
                    answers.update(self.zones[zone].send_query(zone, dns.rdatatype.DNSKEY))
                self.update(answers)
                # Add DS for child zones
                self.__get_child_ds(length, zones, zone)

    def process_query(self, name, rdtype):
        """ Add query to steps and resolve it.

        @param name: Domain name
        @type name: string

        @param rdtype: RDtype
        @type rdtype: integer
        """
        request = dns.message.make_query(name, rdtype)
        response = dns.message.make_response(request, True)
        self.steps.add_query(request)
        self.steps.add_query(response)
        self.resolve_all(dns.name.from_text(name), dns.rdatatype.from_text(rdtype))

    def process_file_txt(self, file):
        """ Process text input

        @param file: Path to a file with DNS query on each line
        @type file: string
        """
        # Get IPs for root
        file_txt = open(file, "r")
        queries = file_txt.readlines()
        for index, query in enumerate(queries):
            try:
                if len(query.split()) != 2:
                    raise ValueError("Wrong number of items ({}), dname and rdatatype expected"
                                     .format(len(query.split())))
                dname = query.split()[0]
                rdtype = query.split()[1]
                rdatatype = dns.rdatatype.from_text(rdtype)
                self.process_query(dname, rdatatype)
            except (ValueError, dns.rdatatype.UnknownRdatatype):
                common.log().exception("Invalid input format line %d: %s", index+1, query)
                common.root_log().error("Invalid input format line %d: %s in file %s",
                                        index + 1, query, common.get_name())
                progressbar.streams.flush()

        self.finalize()

    def finalize(self):
        """ Finalize the scenarios content for finishing the scenario.

        Resolve all names of name servers, add per zone content, update steps rrsets.

        Merge groups to avoid inconsistency.

        @attention: Improvements needed.
        @todo: Current merging of the groups does not take into account all cases.
        @todo: Prioritize when merging groups (best match to the query) - Answer vs. Refused
        @todo: New names from finalizing groups cause endless loop or break the scenario
        """

        if not self.steps.queries or not self.steps.answers:
            raise Exception("No steps found")

        self.resolve_ns()
        self.per_zone_content()
        self.steps.new_rrsets(self)

        answers = set()
        for zone in self.zones.values():
            answers.update(zone.finalize())
        if answers:
            self.update(answers)
            self.finalize()
            return
        # Merge groups with common IP to avoid SERVFAIL
        used = False
        new_groups = list()
        for group in self.groups:
            for other in new_groups:
                if group.ips & other.ips or group.zones & other.zones:
                    used = True
                    other.names.update(group.names)
                    other.update(names_ips=group.names_ips)
                    other.content.update(group.content)
                    other.zones.update(group.zones)
            if not used:
                new_groups.append(group)
            used = False
        self.groups = new_groups
        self.update(set())

        for group in self.groups:
            group.finalize()


def get_scenario(source, output, log_file, no_ipv6, one_server, verbose, text):
    """ Generate scenario

    Setup logging, generate scenario and save it.

    @param source: Path to source file
    @type source: string

    @param output: Path to output file or "stdout"
    @type output: string

    @param log_file: Path to logging file, or empty string
    @type log_file: string

    @param no_ipv6: IPv6 setting for turning IPv6 off
    @type no_ipv6: boolean

    @param one_server: One server setting (Require response from only one server)
    @type one_server: boolean

    @param verbose: Verbosity setting (Log to stdout)
    @type verbose: boolean

    @param text: Source is a text format
    @type text: boolean

    @return: 0 if successful, 1 if error
    @rtype: integer
    """
    name = os.path.basename(source)
    formatter = logging.Formatter('%(levelname)s[%(name)s] %(funcName)s (%(module)s): %(message)s')
    threading.currentThread().setName(name)
    # Prepare logging
    if verbose:
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        logging.getLogger(name).addHandler(handler)

    if log_file == "":
        handler = logging.NullHandler()
    else:
        handler = logging.FileHandler(log_file, mode="w")

    handler.setFormatter(formatter)
    logging.getLogger(name).addHandler(handler)
    logging.getLogger(name).setLevel(logging.DEBUG)
    # Generate scenario
    scenario = None
    # noinspection PyBroadException
    try:
        scenario = Scenario(no_ipv6=no_ipv6, one_server=one_server)
        # Choose source type
        if text:
            scenario.process_file_txt(source)
        else:
            scenario.process_file(source)
        scenario.name = source
        # Return scenario
        if output != "stdout":
            file = open(output, "w")
            file.write(scenario.to_string())
            file.close()
            if verbose:
                common.log().info("Result successfully written into %s", output)
        else:
            common.log().info('Result:\n %s', scenario.to_string())
    except Exception:
        if scenario:
            scenario.log()
        common.log().exception("Exception generating scenario")
        common.root_log().error("Generating scenario from file %s failed", common.get_name())
        return 1
    return 0
