#!/usr/bin/env python3
"""
Server's content
================
"""
from scenario_generator.common import log, is_link
from scenario_generator.message import HashableMessage
from scenario_generator.query import Query


class Server:
    """ DNS name server class for Deckard scenarios

    Contains servers content and separates it into links by labels
    (delegations) and answers.

    @ivar ip_address: Servers IP address
    @type ip_address: string

    @ivar name: Servers name
    @type name: dns.name.Name

    @ivar links: DNS responses with delegation (at the end, more generic are further)

                 Sets of responses are addressed by number of labels they have
    @type links: dictionary

    @ivar answered: Responses with concrete answer
    @type answered: set

    @ivar present: List of all present content (both links and answered), question section as string
    @type present: set
    """
    def __init__(self, ip_addr=None, name=None):
        self.ip_address = ip_addr
        self.name = name
        # Content
        self.links = dict()
        self.answered = set()
        # Answered questions present in the server
        self.present = set()

    def __str__(self):
        """ Get printable list of servers content.

        @return: L{Server}
        @rtype: string
        """
        links_number = 0
        for links_group in self.links.values():
            links_number += len(links_group)
        string = "IP: {0}\n" \
                 "Name: {1}\n" \
                 "Links: {2}\n" \
                 "Answered: {3}," \
                 "Present {4}\n".format(self.ip_address, self.name, links_number,
                                        len(self.answered), len(self.present))
        return string

    def to_string(self):
        """ Create string representation of server (Deckard's range)

        @return: Server as Deckard's RANGE.
        @rtype: string
        """
        server_string = ''
        server_string += '; Server name: ' + str(self.name)
        server_string += " Actual ip: " + self.ip_address + '\n'
        server_string += 'RANGE_BEGIN {0} {1}\n'.format(0, 1000)
        server_string += '\tADDRESS {0}\n\n'.format(self.ip_address)
        server_string += self.to_string_content()
        for qry in self.answered:
            query = Query(query=qry, step=0, link=False)
            server_string += query.to_string(True) + "\n\n"
        # Links - if not answered, find closest link (more labels first)
        for length in sorted(self.links.keys(), key=int, reverse=True):
            for qry in self.links[length]:
                query = Query(query=qry, step=0, link=True)
                server_string += query.to_string(True) + "\n\n"
        server_string += 'RANGE_END\n'
        return server_string

    def to_string_content(self):
        """ Return content of the server in Deckard format as a string

        @return: Servers content only
        @rtype: string
        """
        server_string = ""
        # Precise answers first:
        for qry in self.answered:
            query = Query(query=qry, step=0, link=False)
            server_string += query.to_string(True) + "\n\n"
        # Links - if not answered, find closest link (more labels first)
        for length in sorted(self.links.keys(), key=int, reverse=True):
            for qry in self.links[length]:
                query = Query(query=qry, step=0, link=True)
                server_string += query.to_string(True) + "\n\n"
        return server_string

    def add_query(self, query):
        """ Add response from the server.

        If delegation present, put the query to links.

        @param query: Query to add
        @type query: dns.message.Message
        """
        if query.question:
            question = query.question[0].to_text()
        else:
            log().error("DNS query no question: %s", query.to_text())
            return
        if question in self.present:
            return
        else:
            self.present.add(question)

        if is_link(query):
            length = len(query.question[0].name)
            if length not in self.links:
                self.links[length] = set()
            self.links[length].add(HashableMessage(query))
        else:
            self.answered.add(HashableMessage(query))
