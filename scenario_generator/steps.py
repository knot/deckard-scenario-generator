#!/usr/bin/env python3
"""
Scenario steps representation
=============================
"""
import dns.flags
from scenario_generator.query import Query
from scenario_generator import common


class Steps:
    """ Deckard STEPS representation of DNS queries and responses.

    Queries without answers are skipped.

    @ivar queries: DNS questions
    @type queries: list of dns.message.Message

    @ivar answers: DNS answers
    @type answers: list of dns.message.Message

    @ivar other_answers: Alternative RRsets for CNAME's (RRset's located with the CNAME answer)
    @type other_answers: dictionary by DNS question string
     """
    def __init__(self):
        self.queries = []
        self.answers = []
        # Store alternative RRSETS (included in CNAME answer)
        self.other_answers = dict()

    def to_string(self):
        """ Return string representation of STEPS.

        @return: STEPS
        @rtype: string
        """
        step = 1
        steps_string = ""
        for query in self.queries:
            for answer in self.answers:
                if query.is_response(answer):
                    question = answer.question[0].to_text()
                    other = None
                    if question in self.other_answers:
                        other = self.other_answers[question]
                    steps_string += Query(query, step=step).to_string() + '\n'
                    steps_string += Query(answer, other=other, step=step + 1).to_string() + '\n'
                    step += 2
        return steps_string

    def add_query(self, query):
        """ Add step to the list.

        @param query: DNS Query
        @type query: dns.message.Message
        """
        if query.flags & dns.flags.QR:
            self.answers.append(query)
        else:
            self.queries.append(query)

    def rrset_from_response(self, response, scenario, rdtype, original):
        """ Get desired rrset to a question from response (follow CNAME)

        @param response: Found authoritative answer with matching name and rdtype
        @param response: dns.message.Message

        @param scenario: Scenario object for looking for the responses
        @type scenario: Scenario

        @param rdtype: Required rdtype of the RRset
        @type rdtype: integer

        @param original: Original question looked for before CNAME was found
        @type original: string or None

        @return: New answer RRset for CHECK ANSWER
        @rtype: list
        """
        answer = list()
        # Is there CNAME record?
        cname = common.get_rrset(response, rdatatype=dns.rdatatype.CNAME)
        if cname:
            answer.append(cname)
            if not original:
                question = response.question[0].to_text()
            else:
                question = original
            # Find CNAME target records
            rrset = self.find_rrset(scenario, cname[0].target, rdtype, original=question)
            if rrset:
                answer += rrset
            else:
                rrset = common.get_rrset(response, cname[0].target, rdatatype=dns.rdatatype.CNAME)
                if rrset:
                    answer += rrset
            # Get required records added with CNAME to other answer
            other = common.get_rrset(response, cname[0].target, rdatatype=rdtype)
            if other:
                self.other_answers[question] = other
            return answer
        # If there is no CNAME, add required record
        else:
            rdtype_rrset = common.get_rrset(response, rdatatype=rdtype)
            if rdtype_rrset:
                answer.append(rdtype_rrset)
                return answer
            elif response.answer:
                return response.answer
        return answer

    def find_rrset(self, scenario, name, rdtype, original=None):
        """ Find desired rrset to a question in servers (follow CNAME)

        @param scenario: Scenario object for looking for the responses
        @type scenario: Scenario

        @param name: Required name of the RRset
        @type name: dns.name.Name

        @param rdtype: Required rdtype of the RRset
        @type rdtype: integer

        @param original: Original question looked for before CNAME was found
        @type original: string or None

        @return: New answer RRset for CHECK ANSWER
        @rtype: list
        """
        for server in scenario.servers.values():
            for response in server.answered:
                if response.question[0].name == name and\
                   response.question[0].rdtype == rdtype and\
                   response.flags & dns.flags.AA:
                    return self.rrset_from_response(response, scenario, rdtype, original)
        return list()

    def new_rrsets(self, scenario):
        """ Adjust STEPS CHECK ANSWER to correspond with the RANGE content.

        First query default resolver to get RCODE and FLAGS and than use rrset from provided servers to predict
        the resolvers response as much as possible

        @param scenario: The Scenario to look for the RRsets
        @type scenario: Scenario
        """
        for answer in self.answers:
            response = common.resolver(answer.question[0].name, answer.question[0].rdtype)
            if response:
                answer.flags = response.flags
                answer.answer = response.answer
                answer.additional = response.additional
                answer.authority = response.authority
            new = self.find_rrset(scenario, answer.question[0].name, answer.question[0].rdtype)
            # If rrset found - Check the RCODE
            if new:
                answer.answer = new
