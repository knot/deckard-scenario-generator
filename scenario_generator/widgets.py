#!/usr/bin/env python3
"""
Progressbar widgets
===================
"""
import progressbar


def capture_widgets_build():
    """ Prepare docker build progressbar widgets """
    return [
        progressbar.Percentage(), " (",
        progressbar.SimpleProgress(), ") | ",
        progressbar.Timer(), " | ",
        progressbar.ETA(),
    ]


def capture_widgets_run():
    """ Prepare traffic capture progressbar widgets """
    text = progressbar.FormatCustomText(
        'Last: %(browser)s %(resolver)s %(domain)s %(number)d - %(fail)s',
        dict(
            browser="No",
            resolver="item",
            domain="finished",
            number=0,
            fail="OK"
        ),
    )

    widgets = [
        progressbar.Percentage(), " (",
        progressbar.SimpleProgress(), ") | ",
        progressbar.Timer(), " | ",
        progressbar.ETA(), " | ",
        text,
    ]

    return text, widgets


def generate_scenarios_widgets():
    """ Prepare traffic capture progressbar widgets """
    text = progressbar.FormatCustomText(
        'Last: %(file)s',
        dict(
            file="None"
        ),
    )

    widgets = [
        progressbar.Percentage(), " (",
        progressbar.SimpleProgress(), ") | ",
        progressbar.Timer(), " | ",
        progressbar.ETA(), " | ",
        text,
    ]

    return text, widgets
