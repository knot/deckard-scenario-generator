#!/usr/bin/env python3
"""
Zone of authority
=================
"""
import copy
import dns
from scenario_generator import common


class Zone:
    """ This class stores content for one zone. It is determined by delegations found while resolving.
        It also holds information about DNSSEC chain of trust and name server groups authoritative for this zone.

        @ivar name: The zone's origin
        @type name: dns.name.Name

        @ivar content: Dictionary with dns.message.Message objects addressed by DNS query as string
        @type content: dictionary

        @ivar groups: List of groups of name servers authoritative for this zone.
        @type groups: set

        @ivar signed: Is this zone signed?
        @type signed: boolean

        @ivar signed_child: List of signed child zones
        @type signed_child: set

        @ivar one_server: One server setting
        @type one_server: boolean
    """
    def __init__(self, name, content=None, groups=None, one_server=True):
        self.name = name
        self.signed = False
        self.signed_child = set()
        self.content = content if content else dict()
        self.groups = groups if groups else set()
        self.one_server = one_server

    def __str__(self):
        """ Get string representation of the zones content (not a scenario format, used for debugging)

        @return: List of content of the zone
        @rtype: string
        """
        string = "Zone {}:\n\tSigned: {}\nSigned children:\n".format(self.name, self.signed)
        for child in self.signed_child:
            string += "\t{}\n".format(child)
        string += "Content:\n"
        for dname in sorted(self.content):
            string += "\t\t{0}\n".format(dname)
        string += "\tServer groups:\n"
        for group in self.groups:
            string += "\t\t" + "\t\t".join(str(group).splitlines(True)) + "\n"
        return string

    def to_string(self):
        """ Return RANGE for this zone as a string.

        @deprecated: Group.to_string() is used instead.
        @todo: Rework it so that it works.

        @return: String representation of a zone
        @rtype: string
        """
        string = ""
        for group in self.groups:
            string += group.to_string()
        return string

    def add_content(self, question, query):
        """ Add query to the content list.

        @param question: DNS question
        @type question: string

        @param query: DNS query for the question
        @type query: dns.message.Message
        """
        if question not in self.content.keys():
            self.content[question] = copy.deepcopy(query)

    def send_query(self, name, rdtype=dns.rdatatype.NS, aa_only=False):
        """ Send required query to one or more name servers and return the answer.

        @param name: Required name
        @type name: dns.name.Name

        @param rdtype: Required rdtype
        @type rdtype: integer

        @param aa_only: Accept the response and add the question to the content only if answer had AA flag set to 1.
        @type aa_only: boolean

        @return: Received responses
        @rtype: set of dns.message.Message
        """
        answers = set()
        # Make query and resolve
        question = dns.rrset.RRset(name, dns.rdataclass.IN, rdtype).to_text()
        if question in self.content:
            return answers
        query = dns.message.make_query(name, rdtype, want_dnssec=True)
        # Get response from one of the groups
        for group in self.groups:
            # Get an answer from a group. Only one is taken into consideration.
            answers.update(group.send_query(query, aa_only=aa_only))
        # Check if DNSSEC information is present
        if rdtype is dns.rdatatype.DS or rdtype is dns.rdatatype.DNSKEY:
            for answer in answers:
                ds_record = common.get_rrset(answer, rdatatype=rdtype)
                if ds_record:
                    if ds_record.name != self.name:
                        self.signed_child.add(ds_record.name)
                    else:
                        self.signed = True
        # Check if response received
        if not answers and not aa_only:
            common.log().debug("No response from zone: " + self.name.to_text() +
                               " to query " + query.question[0].to_text())
        elif answers:
            if question not in self.content:
                self.content[question] = query
        return answers

    def finalize(self):
        """ Finalize the state of the zone for saving the scenario.

        Make sure all necessary content is present.

        @return: Answers received while finalizing the content.
        @rtype: set
        """
        answers = set()
        for group in self.groups:
            group.zones.add(self.name)
            for question, query in self.content.items():
                if question not in group.content:
                    answers.update(group.send_query(query))
        return answers
