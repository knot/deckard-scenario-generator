#!/usr/bin/env python3
""" Scenario Generator
    ==================

    Generated scenarios from file or folder with source files.
    Source files are PCAPS with DNS traffic with resolver's ip "127.0.0.1" or text file with queries on each line.
    Uses only one name server as default.

        >>> ./sgen.py -h
        usage: SGEN [-h] [-l DIR] [-a] [-6] [-v] [-t] source [output]
        positional arguments:
            source             file or directory with files, PCAP or TXT with "dname rdtype" on each line format
            output             Output directory (stdout as default)
        optional arguments:
            -h, --help         show this help message and exit
            -l DIR, --log DIR  Logs directory
            -a, --all          Use all name servers per zone
            -6, --ipv6         Disable IPv6
            -v, --verbose      Verbose output, overwrites logging
            -t, --text         Source is a text file (dname rdtype)

"""
import os
import argparse
import concurrent.futures
import logging
import progressbar
from scenario_generator import common, widgets
from scenario_generator.scenario import get_scenario


def process_argv():
    """ Process program arguments

    @todo: logging level, only default output should be number of failed scenarios
    """
    parser = argparse.ArgumentParser(prog='SGEN')
    parser.add_argument("source", help="file or directory with files, PCAP or TXT with dname rdtype lines")
    parser.add_argument("output", nargs='?', default="stdout", help="Output directory (stdout as default)")
    parser.add_argument("-l", "--log", metavar="DIR", default="", help="Logs directory")
    parser.add_argument("-a", "--all", action='store_false', help="Use all name servers per zone")
    parser.add_argument("-6", "--ipv6", action='store_true', help="Disable IPv6")
    parser.add_argument("-v", "--verbose", action='store_true', help="Verbose output, overwrites logging")
    parser.add_argument("-t", "--text", action="store_true", help="Source is a text file (dname rdtype)")
    args = parser.parse_args()

    return args


def generate_scenario(file, args):
    """ Prepare and generate scenario.

    @param file: Path to the source file
    @type file: string

    @param args: Scripts settings
    @type args: namespace

    @return: 0 if successful
    """
    output_file = os.path.join(args.output, file.replace("pcap" if not args.text else "txt", "rpl"))
    file_full = os.path.join(args.source, file)
    if args.log:
        file_log = os.path.join(args.log, file.replace("pcap" if not args.text else "txt", "log"))
    else:
        file_log = args.log

    ret = get_scenario(file_full, output_file, file_log, args.ipv6, args.all, args.verbose, args.text)

    if file_log and os.stat(file_log).st_size == 0:
        os.remove(file_log)

    return ret


def generate_scenarios(args):
    """ Generate scenarios from input

    @param args: Parsed command line arguments
    @type args: namespace

    @return: Number of failed domains
    @rtype: integer
    """
    # Prepare logging
    formatter = logging.Formatter('%(levelname)s[%(name)s] %(funcName)s (%(module)s): %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logging.getLogger("root").addHandler(handler)
    # Prepare output
    if common.create_dir(args.output) != 0:
        print("Output directory can't be created and doesn't exist.")
        exit(1)
    if args.log and common.create_dir(args.log) != 0:
        print("Log directory can't be created and doesn't exist.")
        exit(1)
    if os.path.isdir(args.source):
        files = sorted(os.listdir(args.source))
    else:
        files = [os.path.basename(args.source)]
        args.source = os.path.dirname(args.source)
    # Prepare progressbar
    text, wid = widgets.generate_scenarios_widgets()
    pbar = progressbar.ProgressBar(max_value=len(files), redirect_stdout=True, widgets=wid)
    pbar.start()
    i = 1
    # Failed scenario counter
    failed = 0
    # Run as many scenario generating as possible in parallel
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future_to_file = {executor.submit(generate_scenario, file, args): file for file in files}
        for future in concurrent.futures.as_completed(future_to_file):
            try:
                failed += future.result()
            except Exception as e:
                if args.verbose:
                    common.root_log().exception(" Exception occurred")
                else:
                    common.root_log().error(e)
            # When one of the scenarios is finished, update the progressbar
            text.update_mapping(file=future_to_file[future])
            pbar.update(i)
            i += 1
    pbar.finish()
    if failed:
        common.root_log().error("%d scenario(s) failed", failed)
    return failed


def main():
    """ Run scenario generator """
    # Prepare capture
    args = process_argv()
    # Generate scenarios
    exit(generate_scenarios(args))


if __name__ == "__main__":
    main()
