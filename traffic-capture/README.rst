Traffic capture tools
=====================

This folder contains scripts and files used for traffic capture.

Content:
--------

- ``Dockerfile`` - Dockerfile for building images needed to capture browser traffic in Docker container based on Selenium Dockerfiles extended by all necessary packages and setings for capturing DNS traffic
   - ARG VERSION - set which Selenium standalone image you want to use (which browser)
      - firefox or chrome
   - ARG SOURCES - change the location of scripts folder if needed
   - ARG CONFIGS - change the location of conf folder if needed
- ``Dockerfile-versions`` - File with a list of different versions of Selenium standalone images
- conf folder    - contains configs of the supported resolvers and resolv.conf
- scripts folder - contains scripts that manage running required resolver and browser, and opening required page
   - ``capture.sh``  - bash script runing all the software and scripts in the Docker container
      - ENV BROWSER  - desired browser (requires proper image)
      - ENV RESOLVER - desired resolver (supported resolvers same for all images)
      - ENV PAGE     - web page to be opened
      - ENV NAME     - desired name of the created PCAP
      - ENV RETRY    - how many times should we attept to start the browser again if it failed
      - ENV WAIT     - how long should we wait before attempting to start the browser
      - PCAP is located in the container in folder /capture
   - ``selenium-browsers.py`` - script for opening browsers with selenium

Supported Browsers and Resolvers:
---------------------------------

- Chrome (PC, IOS and Android version), Firefox (PC)
- Bind, Unbound, Knot DNS

Adding Browsers:
----------------
1. Add Browser executable and its dependencies to the Dockerfile
    - Selenium standalone image is required
    - Or the Browser has to be added with its driver (or needs to support headless mode)
2. Add the Browser to capture.sh and create python script for controlling the browser if bash is not enough

Adding Resolvers:
-----------------

1. Add Resolver executable and its dependencies to the Dockerfile
2. Add the Resolver to ``capture.sh``

Building Docker Image:
----------------------

- Build image with command: ``docker build  [--build-arg ARG_NAME=ARG_VALUE] -t image-tag Dockerfile-folder``

Usage:
------

- Run capture with command: ``docker run [ENVIRONMENT VARIABLES] -it image-tag``
   - Use environment variables to get required traffic: -e VARIABLE_NAME=VALUE
      - BROWSER  - desired browser (requires proper image)
      - RESOLVER - desired resolver (supported resolvers same for all images)
      - PAGE     - web page to be opened
      - NAME 	  - desired name of the created PCAP
   a. Copy the PCAP from the container:
       - ``docker cp <container-id>:/capture/<used-name>.pcap /host/path``
   b. Mount directory to /capture folder in the container:
       - ``docker run -v /folder:/capture [ENVIRONMENT VARIABLES] -it image-tag``

Future extensions:
------------------

- Add more browsers
- Add more resolvers


Issues:
-------

- Chrome doesn't start without enough RAM. = Fixed by retries and --no-sendbox option.
