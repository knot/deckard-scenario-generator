#!/usr/bin/env bash
# Parameters
TOOLS=${PWD}
# Files and directories
FILE=$NAME".pcap"
# SETUP DNS RESOLVER
cp $CONFDIR/resolv.conf /etc
if [ "$RESOLVER" = "bind" ]; then
	cp $CONFDIR/named.conf.options /etc/bind
	sudo service bind9 restart
elif [ "$RESOLVER" = "kresd" ]; then
        kresd -a 127.0.0.1 -v -c $CONFDIR/kresd.conf -f 1 /tmp &
elif [ "$RESOLVER" = "unbound" ]; then
	unbound -c $CONFDIR/unbound.conf &
fi

if [ $? != 0 ]; then
	echo "Failed to start resolver"
	exit 1
fi

sudo tcpdump -U -i any -w $FILE port 53 &
sleep 5 # Initialize

ret=0

if [ "$BROWSER" = "firefox" ] || [ "$BROWSER" = "chrome" ] ||
   [ "$BROWSER" = "chrome-android" ] || [ "$BROWSER" = "chrome-ios" ]; then
	$TOOLS/selenium-browsers.py
	ret=$?
elif [ "$BROWSER" = "dig" ]; then
	dig $PAGE
else
	echo "Invalid or unsuported browser"
	ret=1
fi

sleep 5 # extra time to write the output
PID=$(ps -e | pgrep tcpdump)
kill -2 $PID
sleep 5

exit $ret
