#!/usr/bin/env python3
from selenium import webdriver
from xvfbwrapper import Xvfb
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
import sys
import time
import os


def firefox():
    profile = webdriver.FirefoxProfile()

    profile.set_preference("browser.newtabpage.enabled", False)
    profile.set_preference("browser.newtabpage.enhanced", False)
    profile.set_preference("browser.newtabpage.introShown", False)
    profile.set_preference("browser.newtabpage.directory.ping", "")
    profile.set_preference("browser.newtabpage.directory.source", "data:application/json,{}")
    profile.set_preference("browser.newtab.preload", False)
    profile.set_preference("toolkit.telemetry.reportingpolicy.firstRun", False)

    return webdriver.Firefox(profile)


def chrome():
    chrome_options = Options()
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument("--test-type")
    chrome_options.add_argument('no-sandbox')

    return webdriver.Chrome(chrome_options=chrome_options)


def chrome_android():
    mobile_emulation = { "deviceName": "Nexus 5" }
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument("--test-type")
    chrome_options.add_argument('no-sandbox')
    chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)

    return webdriver.Chrome(desired_capabilities = chrome_options.to_capabilities())


def chrome_ios():
    mobile_emulation = { "deviceName": "iPhone 4" }
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument("--test-type")
    chrome_options.add_argument('no-sandbox')
    chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)

    return webdriver.Chrome(chrome_options=chrome_options)


options = {"firefox":firefox,
           "chrome":chrome,
           "chrome-ios":chrome_ios,
           "chrome-android":chrome_android,
}


def main():
    browser = os.environ['BROWSER']

    if browser not in options:
        print('Browser "'+browser+'" not supported', file=sys.stderr)
        sys.exit(1)
    
    display = Xvfb()
    display.start()

    ret = 0
    retry = int(os.environ['RETRY']) + 1
    # Repeat until success or too many retries
    for i in range(retry):
        try:
            webdriver = options[browser]()
            webdriver.get('http://' + os.environ['PAGE'])
            webdriver.quit()
        except Exception as e:
            print(i, e, os.environ['PAGE'])
            if i < retry - 1:
                  time.sleep(int(os.environ['WAIT']))
            else:
                print("Failed ", e, os.environ['PAGE'], file=sys.stderr)
                ret = 2
                break
        else:
            break

    display.stop()
    return ret


if __name__ == "__main__":
    sys.exit(main())
